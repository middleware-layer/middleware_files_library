#####################################################################
# File: CMakeLists.txt                                              #
#                                                                   #
# Project Name: Middleware_Simulation_Library                       #
#                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com) #
#####################################################################

coloredMessage(BoldYellow "Loading Documentation CMakeLists" STATUS)

set(DOC_APP_NAME ${PROJECT_FULLNAME}-doxygen)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

#############################################
#   A.1. Set Options                        #
#############################################

set(DOXYGEN_IN          ${PROJECT_DOCUMENTATION_FOLDER}/Doxygen/Doxyfile)
set(DOXYGEN_OUTPUT_DIR  ${PROJECT_ROOT_FOLDER})

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

find_package(Doxygen REQUIRED)

if(DOXYGEN_FOUND)
    coloredMessage(BoldYellow "Doxygen build started" STATUS)

    # note the option ALL which allows to build the docs together with the application
    add_custom_target(${DOC_APP_NAME} ALL
                      COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_IN}
                      WORKING_DIRECTORY ${DOXYGEN_OUTPUT_DIR}
                      COMMENT "Generating API documentation with Doxygen"
                      VERBATIM)
else()
    coloredMessage(BoldRed "Doxygen need to be installed to generate the doxygen documentation" FATAL_ERROR)
endif()

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################
