#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Files_Library                                             #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

include(ProjectConf)

###############################
### GENERAL CONFIGURATIONS  ###
###############################

if(USE_COTIRE)
    # We use cotire, simply include it
    coloredMessage(BoldBlue "using cotire" STATUS)
    include(cotire)
else()
    # We do not use cotire, create dummy function.
    coloredMessage(BoldBlue "not using cotire" STATUS)
    function(cotire)
    endfunction(cotire)
endif()

if(USE_BOOST)
    # We use Boost, simply find and include it
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_RUNTIME OFF)
    find_package(Boost 1.45.0 REQUIRED)

    if(Boost_FOUND)
        include_directories(${Boost_INCLUDE_DIRS})
        add_executable(progname file1.cxx file2.cxx)
        target_link_libraries(progname ${Boost_LIBRARIES})
    endif()
endif()

if(${STATICLIB_SWITCH} STREQUAL "ON")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -static -Wl,--whole-archive -lpthread -Wl,--no-whole-archive")
  set(CMAKE_FIND_LIBRARY_SUFFIXES ".a;.so")
endif()

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

string(FIND "${EXTERNAL_DEFINES}" "MIDDLEWARE_USE_STM32F7" retVal1)
if(${retVal1} EQUAL -1)
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_MIDDLEWARE}")
endif()

#############################################
###             MBED CONFIGURATIONS       ###
#############################################

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_ANALOGIN" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_ANALOGIN} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ANALOGIN=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ANALOGIN=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_ANALOGOUT" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_ANALOGOUT} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ANALOGOUT=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ANALOGOUT=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_CAN" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_CAN} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_CAN=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_CAN=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_ETHERNET" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_ETHERNET} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ETHERNET=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_ETHERNET=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_FLASH" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_FLASH} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_FLASH=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_FLASH=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_I2C" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_I2C} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2C=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2C=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_I2C_ASYNCH" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_I2C_ASYNCH} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2C_ASYNCH=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2C_ASYNCH=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_I2CSLAVE" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_I2CSLAVE} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2CSLAVE=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_I2CSLAVE=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_INTERRUPTIN" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_INTERRUPTIN} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_INTERRUPTIN=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_INTERRUPTIN=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_LOWPOWERTIMER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_LOWPOWERTIMER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_LOWPOWERTIMER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_LOWPOWERTIMER=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_PORTIN" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_PORTIN} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTIN=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTIN=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_PORTINOUT" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_PORTINOUT} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTINOUT=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTINOUT=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_PORTOUT" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_PORTOUT} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTOUT=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PORTOUT=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_PWMOUT" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_PWMOUT} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PWMOUT=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_PWMOUT=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_RTC" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_RTC} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_RTC=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_RTC=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SERIAL" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SERIAL} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SERIAL_ASYNCH" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SERIAL_ASYNCH} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL_ASYNCH=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL_ASYNCH=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SERIAL_FC" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SERIAL_FC} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL_FC=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SERIAL_FC=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SLEEP" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SLEEP} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SLEEP=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SLEEP=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SPI" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SPI} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPI=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPI=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SPI_ASYNCH" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SPI_ASYNCH} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPI_ASYNCH=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPI_ASYNCH=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_SPISLAVE" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_SPISLAVE} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPISLAVE=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_SPISLAVE=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_STDIO_MESSAGES" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_STDIO_MESSAGES} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_STDIO_MESSAGES=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_STDIO_MESSAGES=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "DEVICE_TRNG" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_DEVICE_TRNG} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_TRNG=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DDEVICE_TRNG=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE" retVal)

if(${retVal} EQUAL -1)
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DMBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE=${DEFAULT_SERIAL_BAUD_RATE}")
endif()

###############################
###  BOARD CONFIGURATIONS   ###
###############################

string(FIND "${EXTERNAL_DEFINES}" "USE_ADAFRUIT_SHIELD" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F7xx_NUCLEO_144" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_DISCO" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "USE_STM32F769I_EVAL" retVal4)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Board_Platform}")
endif()

###########################################
###          CMSIS CONFIGURATIONS       ###
###########################################

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Building_Library}")
endif()

string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal1)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal2)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal3)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal4)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal5)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MBL" retVal6)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_ARMV8MML" retVal7)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM7" retVal8)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM4" retVal9)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM3" retVal10)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0" retVal11)
string(FIND "${EXTERNAL_DEFINES}" "ARM_MATH_CM0PLUS" retVal12)

if((${retVal1} EQUAL -1) AND (${retVal2} EQUAL -1) AND (${retVal3} EQUAL -1) AND (${retVal4} EQUAL -1) AND
   (${retVal5} EQUAL -1) AND (${retVal6} EQUAL -1) AND (${retVal7} EQUAL -1) AND (${retVal8} EQUAL -1) AND
   (${retVal9} EQUAL -1) AND (${retVal10} EQUAL -1) AND (${retVal11} EQUAL -1) AND (${retVal12} EQUAL -1))
    set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -D${DEFAULT_Processor_Series}")
endif()

#############################################
###          HAL CONFIGURATIONS           ###
#############################################

string(FIND "${EXTERNAL_DEFINES}" "USE_HAL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_HAL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_HAL_DRIVER=0")
    endif()
endif()

string(FIND "${EXTERNAL_DEFINES}" "USE_FULL_LL_DRIVER" retVal)

if(${retVal} EQUAL -1)
    if(${DEFAULT_USE_FULL_LL_DRIVER} STREQUAL "ON")
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=1")
    else()
        set(PROJECT_CONFIG_DEFINES "${PROJECT_CONFIG_DEFINES} -DUSE_FULL_LL_DRIVER=0")
    endif()
endif()

####################################
### FILES LIBRARY CONFIGURATIONS ###
####################################

if(LIBHPDF_HAVE_NOPNGLIB)
    coloredMessage(BoldBlue "using LIBHPDF_HAVE_NOPNGLIB" STATUS)

    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DLIBHPDF_HAVE_NOPNGLIB")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DLIBHPDF_HAVE_NOPNGLIB")
endif()

if(LIBHPDF_HAVE_NOZLIB)
    coloredMessage(BoldBlue "using LIBHPDF_HAVE_NOZLIB" STATUS)

    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -DLIBHPDF_HAVE_NOZLIB")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DLIBHPDF_HAVE_NOZLIB")
endif()
