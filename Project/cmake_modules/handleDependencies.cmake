#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Files_Library                                             #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

################################################
# In this file one can select the dependencies #
# needed for this project                      #
################################################

#############################################
# 1. Google Test / Mock                     #
#############################################

# We should just look for GTest and GMock if compiling for 'Stubs' or 'Testing'
if((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
	include (FindGTest)
endif()

#############################################
# 2. STM32F7_Driver_Layer                   #
#############################################

include (FindSTM32F7_Driver_Layer)

#############################################
# 3. Middleware_MBedOS_Library              #
#############################################

include (FindMiddleware_MBedOS_Library)
