#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Files_Library                                             #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

###############################
### GENERAL CONFIGURATIONS  ###
###############################

option(USE_COTIRE                   "Use the COmpilation TIme REducer."                     ON)
option(USE_BOOST                    "Use the Boost Library."                                OFF)
option(STATICLIB_SWITCH             "Compile a statically linked version of the library."   OFF)

option(LIBHPDF_HAVE_NOPNGLIB        "If PDF has no support to PNG images, turn this option off" ON)
option(LIBHPDF_HAVE_NOZLIB          "If PDF has support to ZLib, turn this option on" ON)

#########################################
### GENERAL MIDDLEWARE CONFIGURATIONS ###
#########################################

set(DEFAULT_MIDDLEWARE                  "MIDDLEWARE_USE_STM32F7")

#############################################
###             MBED CONFIGURATIONS       ###
#############################################

set(DEFAULT_DEVICE_ANALOGIN             ON)
set(DEFAULT_DEVICE_ANALOGOUT            ON)
set(DEFAULT_DEVICE_CAN                  ON)
set(DEFAULT_DEVICE_ETHERNET             ON)
set(DEFAULT_DEVICE_FLASH                ON)
set(DEFAULT_DEVICE_I2C                  ON)
set(DEFAULT_DEVICE_I2C_ASYNCH           ON)
set(DEFAULT_DEVICE_I2CSLAVE             ON)
set(DEFAULT_DEVICE_INTERRUPTIN          ON)
set(DEFAULT_DEVICE_LOWPOWERTIMER        ON)
set(DEFAULT_DEVICE_PORTIN               ON)
set(DEFAULT_DEVICE_PORTINOUT            ON)
set(DEFAULT_DEVICE_PORTOUT              ON)
set(DEFAULT_DEVICE_PWMOUT               ON)
set(DEFAULT_DEVICE_RTC                  ON)
set(DEFAULT_DEVICE_SERIAL               ON)
set(DEFAULT_DEVICE_SERIAL_ASYNCH        ON)
set(DEFAULT_DEVICE_SERIAL_FC            ON)
set(DEFAULT_DEVICE_SLEEP                ON)
set(DEFAULT_DEVICE_SPI                  ON)
set(DEFAULT_DEVICE_SPI_ASYNCH           ON)
set(DEFAULT_DEVICE_SPISLAVE             ON)
set(DEFAULT_DEVICE_STDIO_MESSAGES       ON)
set(DEFAULT_DEVICE_TRNG                 ON)

set(DEFAULT_SERIAL_BAUD_RATE            "9600")

###############################
###  BOARD CONFIGURATIONS   ###
###############################

#set(DEFAULT_Board_Platform              USE_ADAFRUIT_SHIELD)
#set(DEFAULT_Board_Platform              USE_STM32F7xx_NUCLEO_144)
set(DEFAULT_Board_Platform              "USE_STM32F769I_DISCO")
#set(DEFAULT_Board_Platform              USE_STM32F769I_EVAL)

###########################################
###          CMSIS CONFIGURATIONS       ###
###########################################

set(DEFAULT_Building_Library            "ARM_MATH_CM7")
#set(DEFAULT_Building_Library            ARM_MATH_CM4)
#set(DEFAULT_Building_Library            ARM_MATH_CM3)
#set(DEFAULT_Building_Library            ARM_MATH_CM0)
#set(DEFAULT_Building_Library            ARM_MATH_CM0PLUS)
#set(DEFAULT_Building_Library            ARM_MATH_ARMV8MBL)
#set(DEFAULT_Building_Library            ARM_MATH_ARMV8MML)

#set(DEFAULT_Processor_Series            STM32F722xx)
#set(DEFAULT_Processor_Series            STM32F723xx)
#set(DEFAULT_Processor_Series            STM32F732xx)
#set(DEFAULT_Processor_Series            STM32F733xx)
#set(DEFAULT_Processor_Series            STM32F745xx)
#set(DEFAULT_Processor_Series            STM32F746xx)
#set(DEFAULT_Processor_Series            STM32F756xx)
#set(DEFAULT_Processor_Series            STM32F765xx)
#set(DEFAULT_Processor_Series            STM32F767xx)
set(DEFAULT_Processor_Series            "STM32F769xx")
#set(DEFAULT_Processor_Series            STM32F777xx)
#set(DEFAULT_Processor_Series            STM32F779xx)

#############################################
###          HAL CONFIGURATIONS           ###
#############################################

set(DEFAULT_USE_HAL_DRIVER              ON)
set(DEFAULT_USE_FULL_LL_DRIVER          OFF)
