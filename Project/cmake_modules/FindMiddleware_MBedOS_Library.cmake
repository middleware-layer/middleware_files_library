#####################################################################################
# File: FindMiddleware_MBedOS_Library.cmake                                         #
#                                                                                   #
# Project Name: Midleware_Files_Library                                             #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

if(DEFINED MIDDLEWARE_MBEDOS_LIBRARY_PROJ_NAME)
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
    set(MIDDLEWARE_MBEDOS_LIBRARY_PROJ_NAME                  "Middleware_MBedOS_Library")
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
    set(MIDDLEWARE_MBEDOS_LIBRARY_PROJ_NAME                  "Middleware_MBedOS_Library-Stubs")
endif()

set(MIDDLEWARE_MBEDOS_GIT_PROJ_NAME                     "middleware_mbedos_library")
set(MIDDLEWARE_MBEDOS_LIBRARY_GIT_REPOSITORY            "https://gitlab.com/middleware-layer/middleware_mbedos_library.git")
set(MIDDLEWARE_MBEDOS_LIBRARY_GIT_TAG                   "v1.0.0")

set(MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_ROOT_FOLDER       "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${MIDDLEWARE_MBEDOS_GIT_PROJ_NAME}-src/")

if(DEFINED MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER)
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
    set(MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${MIDDLEWARE_MBEDOS_GIT_PROJ_NAME}-src/build/${CMAKE_BUILD_TYPE}/Source/Production/")
elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
    set(MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER        "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${MIDDLEWARE_MBEDOS_GIT_PROJ_NAME}-src/build/${PROJECT_STUBS_BUILD_NAME}/Source/Testing/Stubs/")
endif()

if(NOT DEFINED MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_INCLUDE_FOLDER)
    set(MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_INCLUDE_FOLDER    "${PROJECT_ROOT_FOLDER}/build/${CMAKE_BUILD_TYPE}/${MIDDLEWARE_MBEDOS_GIT_PROJ_NAME}-src/Source/")
endif()

include(DownloadProject)

function(check_middleware_mbedos_library_dependency)
    coloredMessage(BoldMagenta "Well, it seems I need Middleware MBedOS Library. Let's see if I can find it!" STATUS)

    find_library(MIDDLEWARE_MBEDOS_LIBRARY_LIB
                 NAMES      ${MIDDLEWARE_MBEDOS_LIBRARY_PROJ_NAME}
                 HINTS      ${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER}
                 PATHS      ${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER})

    if(MIDDLEWARE_MBEDOS_LIBRARY_LIB)
        coloredMessage(BoldMagenta "We have the libraries, let me just include the directory and link them to you (:" STATUS)
    else()
        coloredMessage(BoldMagenta "I couldn't find them, let me download and compile it for you (:" STATUS)

        #First download the project
        download_project(PROJ                ${MIDDLEWARE_MBEDOS_GIT_PROJ_NAME}
                         GIT_REPOSITORY      ${MIDDLEWARE_MBEDOS_LIBRARY_GIT_REPOSITORY}
                         GIT_TAG             ${MIDDLEWARE_MBEDOS_LIBRARY_GIT_TAG}
                         ${UPDATE_DISCONNECTED_IF_AVAILABLE}
                         QUIET)

        set(cmd_gradle  "gradle")

        if((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
            set(arg         "build${CMAKE_BUILD_TYPE}")
        elseif(${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME})
            set(arg         "build${PROJECT_STUBS_BUILD_NAME}")
        endif()

        set(external_args   "-Pexternal_defines='${COMPILER_PROJECT_CONFIGS}'")

        if((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_DEBUG_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_RELEASE_BUILD_NAME}))
            set(stm32f7_configs_proj_name  "-Pstm32f7_driver_layer_configs_proj_name='${STM32F7_DRIVER_LAYER_PROJ_NAME}'")
            set(stm32f7_configs_lib_folder "-Pstm32f7_driver_layer_configs_lib_folder='${STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER}'")
            set(stm32f7_configs_inc_folder "-Pstm32f7_driver_layer_configs_inc_folder='${STM32F7_DRIVER_LAYER_PROJECT_INCLUDE_FOLDER}'")

            #Second, compile it
            execute_process(COMMAND             ${cmd_gradle} 
                                                ${external_args} 
                                                ${stm32f7_configs_proj_name} 
                                                ${stm32f7_configs_lib_folder} 
                                                ${stm32f7_configs_inc_folder} 
                                                ${arg}
                            WORKING_DIRECTORY   ${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_ROOT_FOLDER}
                            OUTPUT_QUIET)
        elseif((${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_TESTS_BUILD_NAME}) OR (${CMAKE_BUILD_TYPE} STREQUAL ${PROJECT_STUBS_BUILD_NAME}))
            set(gtest_configs_proj_name  "-Pgtest_configs_proj_name='${GTEST_PROJ_NAME}'")
            set(gtest_configs_lib_folder "-Pgtest_configs_lib_folder='${GTEST_PROJECT_LIB_FOLDER}'")
            set(gtest_configs_inc_folder "-Pgtest_configs_inc_folder='${GTEST_PROJECT_INCLUDE_FOLDER}'")
            set(gmock_configs_inc_folder "-Pgmock_configs_inc_folder='${GMOCK_PROJECT_INCLUDE_FOLDER}'")

            set(stm32f7_configs_proj_name  "-Pstm32f7_driver_layer_configs_proj_name='${STM32F7_DRIVER_LAYER_PROJ_NAME}'")
            set(stm32f7_configs_lib_folder "-Pstm32f7_driver_layer_configs_lib_folder='${STM32F7_DRIVER_LAYER_PROJECT_LIB_FOLDER}'")
            set(stm32f7_configs_inc_folder "-Pstm32f7_driver_layer_configs_inc_folder='${STM32F7_DRIVER_LAYER_PROJECT_INCLUDE_FOLDER}'")

            #Second, compile it
            execute_process(COMMAND             ${cmd_gradle} 
                                                ${external_args} 
                                                ${gtest_configs_proj_name} 
                                                ${gtest_configs_lib_folder} 
                                                ${gtest_configs_inc_folder} 
                                                ${gmock_configs_inc_folder} 
                                                ${stm32f7_configs_proj_name} 
                                                ${stm32f7_configs_lib_folder} 
                                                ${stm32f7_configs_inc_folder} 
                                                ${arg}
                            WORKING_DIRECTORY   ${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_ROOT_FOLDER}
                            OUTPUT_QUIET)
        endif()
    endif()

    link_directories(${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_LIB_FOLDER})

    include_directories(${MIDDLEWARE_MBEDOS_LIBRARY_PROJECT_INCLUDE_FOLDER})
endfunction()

function(link_middleware_mbedos_library_libraries targetName)
    target_link_libraries(${targetName}
        lib${MIDDLEWARE_MBEDOS_LIBRARY_PROJ_NAME}.a
    )
endfunction()
