/**
 ******************************************************************************
 * @file    IntelHex_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/IntelHex/IntelHex.hpp>

namespace Files
{
   class IntelHexTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<IntelHex> intelHex;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		 void SetUp(void)
		 {

		 }

		 /**
		  * @brief	This method will run always after each test
		  */
		 void TearDown(void)
		 {

		 }
   };

   TEST_F(IntelHexTest, CheckClassInitialization)
   {
      this->intelHex = std::make_shared<IntelHex>("test",
                                                  IntelHex::File_Operation::WRITE);

      ASSERT_NE(this->intelHex->getFile(),
                nullptr);

      return;
   }

   TEST_F(IntelHexTest, CheckSharedPointerReset)
   {
      this->intelHex = std::make_shared<IntelHex>("test",
                                                  IntelHex::File_Operation::WRITE);

      this->intelHex.reset();

      ASSERT_EQ(this->intelHex,
                nullptr);

      return;
   }

   TEST_F(IntelHexTest, ReadIntelHexRecord_12_Registers)
   {
      IntelHex::IHexErrors error = IntelHex::IHexErrors::IHEX_OK;

      this->intelHex = std::make_shared<IntelHex>("test",
                                                  IntelHex::File_Operation::READ);

      do
      {
         ASSERT_EQ(error,
                   IntelHex::IHexErrors::IHEX_OK);

         error = this->intelHex->readRecord();

         //this->intelHex->printRecord();

         //printf("\n");
      } while(error == IntelHex::IHexErrors::IHEX_OK);

      return;
   }

   TEST_F(IntelHexTest, WriteRecordToFile)
   {
      IntelHex::IHexErrors error = IntelHex::IHexErrors::IHEX_OK;

      this->intelHex = std::make_shared<IntelHex>("testWrite",
                                                  IntelHex::File_Operation::WRITE);

      uint8_t buffer[5] = "";

      for(uint32_t i = 0; i < 20; i++)
      {
         error = this->intelHex->newRecord(IntelHex::RecordTypes::IHEX_TYPE_00,
                                           0,
                                           buffer,
                                           5);

         ASSERT_EQ(error,
                   IntelHex::IHexErrors::IHEX_OK);

         this->intelHex->writeRecord();

         ASSERT_EQ(error,
                   IntelHex::IHexErrors::IHEX_OK);
      }

      return;
   }
}
