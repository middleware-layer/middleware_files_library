/**
 ******************************************************************************
 * @file    XML_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/XML/XML.hpp>

namespace Files
{
   class XMLTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<XML> xml;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		  void SetUp(void)
		 {

		 }

		  /**
		   * @brief	This method will run always after each test
		   */
		  void TearDown(void)
		  {

		  }
   };

   TEST_F(XMLTest, CheckClassInitialization)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::WRITE);

      ASSERT_NE(this->xml->getFile(),
                nullptr);

      return;
   }

   TEST_F(XMLTest, CheckSharedPointerReset)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::WRITE);

      this->xml.reset();

      ASSERT_EQ(this->xml,
                nullptr);

      return;
   }

   TEST_F(XMLTest, CreateFile)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::WRITE);

      // firstly, one should ALWAYS create a root node
      ASSERT_EQ(this->xml->setRootNode("Root"),
                XML::XMLError::OK);

      // The following command is how we add an element to the root node
      ASSERT_EQ(this->xml->writeElement("IntValue",
                                        10),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeElement("FloatValue",
                                        0.5f),
                XML::XMLError::OK);

      //************************************************************
      // Now we will open a date element and add attributes to it //
      //**********************************************************//

      ASSERT_EQ(this->xml->createElement("Date"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Date",
                                                         "day",
                                                         26),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Date",
                                                         "month",
                                                         "April"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Date",
                                                         "year",
                                                         2014),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Date",
                                                         "dateFormat",
                                                         "26/04/2014"),
                XML::XMLError::OK);

      // when all attributes were added, we need to add the element to the root node
      ASSERT_EQ(this->xml->closeElement("Date"),
                XML::XMLError::OK);

      //*************************************
      // Now we will create a list element //
      //*************************************
      ASSERT_EQ(this->xml->createElement("List"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->addChildElementToOpenedElement("List",
                                                          "Item",
                                                          1),
                XML::XMLError::OK);

      std::vector<int32_t> vectorList = {12,
                                         14,
                                         21};

      ASSERT_EQ(this->xml->addChildElementToOpenedElement("List",
                                                          "Item",
                                                          vectorList),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("List",
                                                         "ItemCount",
                                                         4),
                XML::XMLError::OK);

      // when all attributes were added, we need to add the element to the root node
      ASSERT_EQ(this->xml->closeElement("List"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);

      return;
   }

   TEST_F(XMLTest, ReadFile)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, OpenFileAndAddElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeElement("NewValue",
                                        0.875f),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, OpenElementFromFileAndAddAttribute)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->openElement("Date"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Date",
                                                         "weeksOnYear",
                                                         52),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, OpenElementFromFileAndAddElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      //***********************************************
      // We will create a new element inside another //
      //***********************************************
      ASSERT_EQ(this->xml->openElement("Date"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->createElement("Week"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Week",
                                                         "weekdays",
                                                         5),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeAttributeToOpenedElement("Week",
                                                         "weekend",
                                                         2),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->closeElement("Date",
                                        "Week"),
                XML::XMLError::OK);

      // Just remove the element from the map, so we get rid of some bytes of memory
      ASSERT_EQ(this->xml->eraseElementFromMap("Date"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, OpenElementFromFileAndAddChildElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      //***********************************************
      // We will create a new element inside another //
      //***********************************************
      ASSERT_EQ(this->xml->openElement("NewValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->writeElement("NewValue",
                                        "gravity",
                                        9.81),
                XML::XMLError::OK);

      // Just remove the element from the map, so we get rid of some bytes of memory
      ASSERT_EQ(this->xml->eraseElementFromMap("NewValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, ReadElementFromFile)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      float value = 0.0f;

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->getValueFromElement("FloatValue",
                                               &value),
                XML::XMLError::OK);

      ASSERT_FLOAT_EQ(value,
                      0.5f);
   }

   TEST_F(XMLTest, ReadChildElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      double value = 0.0f;

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->openElement("NewValue"),
                XML::XMLError::OK);

      //
      ASSERT_EQ(this->xml->getValueFromChildElement("NewValue",
                                                    "gravity",
                                                    &value),
                XML::XMLError::OK);

      ASSERT_FLOAT_EQ(value,
                      9.81f);

      // Just remove the element from the map, so we get rid of some bytes of memory
      ASSERT_EQ(this->xml->eraseElementFromMap("NewValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, DeleteElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->deleteElement("FloatValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }

   TEST_F(XMLTest, DeleteChildElement)
   {
      this->xml = std::make_shared<XML>("test",
                                        XML::File_Operation::READ);

      ASSERT_EQ(this->xml->readFile(),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->openElement("NewValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->deleteChildElement("NewValue",
                                              "gravity"),
                XML::XMLError::OK);

      // Just remove the element from the map, so we get rid of some bytes of memory
      ASSERT_EQ(this->xml->eraseElementFromMap("NewValue"),
                XML::XMLError::OK);

      ASSERT_EQ(this->xml->saveFile(),
                XML::XMLError::OK);
   }
}
