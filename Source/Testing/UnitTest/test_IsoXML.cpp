/**
 ******************************************************************************
 * @file    IsoXML_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    19 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/IsoXML/IsoXML.hpp>

namespace Files
{
   class IsoXMLTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<IsoXML> isoXML;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		 void SetUp(void)
		 {

		 }

		 /**
		  * @brief	This method will run always after each test
		  */
		 void TearDown(void)
		 {

		 }
   };

   TEST_F(IsoXMLTest, CheckClassInitialization)
   {
      this->isoXML = std::make_shared<IsoXML>("test_isoXML",
                                              IsoXML::File_Operation::WRITE);

      ASSERT_NE(this->isoXML->getFile(),
                nullptr);

      return;
   }

   TEST_F(IsoXMLTest, CheckSharedPointerReset)
   {
      this->isoXML = std::make_shared<IsoXML>("test_isoXML",
                                              IsoXML::File_Operation::WRITE);

      this->isoXML.reset();

      ASSERT_EQ(this->isoXML,
                nullptr);

      return;
   }
}
