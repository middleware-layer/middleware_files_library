/*
 * FooTests.cpp
 *
 *  Created on: 21 de mai de 2018
 *      Author: leonardo
 */

#include <gtest/gtest.h>

#include <Production/Json/JSon.hpp>

namespace Files
{
   class JsonTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<JSon> json;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		 void SetUp(void)
		 {

		 }

		 /**
		  * @brief	This method will run always after each test
		  */
		 void TearDown(void)
		 {

		 }
   };

   TEST_F(JsonTest, CheckClassInitialization)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      ASSERT_NE(this->json->getFile(),
                nullptr);

      return;
   }

   TEST_F(JsonTest, CheckSharedPointerReset)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json.reset();

      ASSERT_EQ(this->json,
                nullptr);

      return;
   }

   TEST_F(JsonTest, CheckEmptyStructure)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      ASSERT_EQ(this->json->isJsonEmpty(),
                true);

      return;
   }

   TEST_F(JsonTest, StringAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      std::string value;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"name"},
                           "Leonardo");

      error = this->json->getEntry( {"name"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_EQ(value,
                "Leonardo");

      return;
   }

   TEST_F(JsonTest, StringArrayAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      std::vector<std::string> value;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"name"},
                           {"Leonardo"});

      this->json->addEntry( {"name"},
                           {"Winter",
                            "Pereira"});

      this->json->addEntry( {"name"},
                           "Germany");

      error = this->json->getEntry( {"name"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      return;
   }

   TEST_F(JsonTest, DoubleAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      double value = 0.0;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"pi"},
                           3.141);

      error = this->json->getEntry( {"pi"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_EQ(value,
                3.141);

      return;
   }

   TEST_F(JsonTest, DoubleArrayAssignmentTest)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"value",
                             "second Layer"},
                           {3.141,
                            6.282});

      this->json->addEntry( {"value",
                             "third Layer"},
                           21.2);

      this->json->addEntry( {"value",
                             "second Layer"},
                           {14.7,
                            18.9});

      return;
   }

   TEST_F(JsonTest, FloatAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      float value = false;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"floatValue"},
                           3.141);

      error = this->json->getEntry( {"floatValue"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_FLOAT_EQ(value,
                      3.141);

      return;
   }

   TEST_F(JsonTest, UnsignedIntegerAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      uint32_t value = false;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"uint32_tValue"},
                           4231556);

      error = this->json->getEntry( {"uint32_tValue"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_EQ(value,
                (uint32_t )4231556);

      return;
   }

   TEST_F(JsonTest, UnsignedIntegerArrayAssignmentTest)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"uint32_tValue"},
                           {3141,
                            6282});

      this->json->addEntry( {"uint32_tValue"},
                           212);

      this->json->addEntry( {"uint32_tValue"},
                           {147,
                            188});

      return;
   }

   TEST_F(JsonTest, SignedIntegerAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      int32_t value = false;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"int32_tValue"},
                           -4231556);

      error = this->json->getEntry( {"int32_tValue"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_EQ(value,
                -4231556);

      return;
   }

   TEST_F(JsonTest, SignedIntegerArrayAssignmentTest)
   {
      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"value"},
                           3141);

      this->json->addEntry( {"moreValues"},
                           {212,
                            44});

      this->json->addEntry( {"values",
                             "second Layer",
                             "third Layer"},
                           147);

      this->json->addEntry( {"values",
                             "second Layer",
                             "third Layer v2"},
                           {147});

      this->json->addEntry( {"values",
                             "second Layer",
                             "third Layer v2"},
                           {147,
                            224});

      return;
   }

   TEST_F(JsonTest, BoolAssignmentTest)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      bool value = false;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"happy"},
                           true);

      error = this->json->getEntry( {"happy"},
                                   value);

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      ASSERT_EQ(value,
                true);

      return;
   }

   TEST_F(JsonTest, TestWriteToFile)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::WRITE);

      this->json->addEntry( {"Person",
                             "Leonardo",
                             "First Name"},
                           "Leonardo");

      this->json->addEntry( {"Person",
                             "Leonardo",
                             "Last Name"},
                           "Winter Pereira");

      this->json->addEntry( {"Person",
                             "Leonardo",
                             "Age"},
                           26);

      error = this->json->writeFile();

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      return;
   }

   TEST_F(JsonTest, TestReadFile)
   {
      JSon::JsonError error = JSon::JsonError::JsonError_OK;

      this->json = std::make_shared<JSon>("test",
                                          JSon::File_Operation::READ);

      error = this->json->readFile();

      ASSERT_EQ(error,
                JSon::JsonError::JsonError_OK);

      return;
   }
}
