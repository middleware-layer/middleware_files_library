/**
 ******************************************************************************
 * @file    PDF_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/PDF/PDF.hpp>

namespace Files
{
   class PDFTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<PDF> pdf;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		 void SetUp(void)
		 {

		 }

		 /**
		  * @brief	This method will run always after each test
		  */
		 void TearDown(void)
		 {

		 }
   };

   TEST_F(PDFTest, CheckClassInitialization)
   {
      this->pdf = std::make_shared<PDF>("test",
                                        PDF::File_Operation::WRITE);

      ASSERT_NE(this->pdf->getFile(),
                nullptr);

      return;
   }

   TEST_F(PDFTest, CheckSharedPointerReset)
   {
      this->pdf = std::make_shared<PDF>("test",
                                        PDF::File_Operation::WRITE);

      this->pdf.reset();

      ASSERT_EQ(this->pdf,
                nullptr);

      return;
   }

   TEST_F(PDFTest, CreateOnePageFile)
   {
      PDF::PDFError error;

      this->pdf = std::make_shared<PDF>("test",
                                        PDF::File_Operation::WRITE);

      error = this->pdf->setDocumentObjectAttributes(HPDF_COMP_ALL,
                                                     HPDF_PAGE_MODE_USE_OUTLINE);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      error = this->pdf->createNewPage(HPDF_PAGE_SIZE_A4,
                                       HPDF_PAGE_LANDSCAPE);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      return;
   }

   TEST_F(PDFTest, CreateMultiplePagesFile)
   {
      PDF::PDFError error;

      this->pdf = std::make_shared<PDF>("test",
                                        PDF::File_Operation::WRITE);

      error = this->pdf->setDocumentObjectAttributes(HPDF_COMP_ALL,
                                                     HPDF_PAGE_MODE_USE_OUTLINE);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      error = this->pdf->createNewPage(HPDF_PAGE_SIZE_A3,
                                       HPDF_PAGE_LANDSCAPE);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      error = this->pdf->insertNewPageAtPosition(HPDF_PAGE_SIZE_A4,
                                                 HPDF_PAGE_LANDSCAPE,
                                                 0);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      return;
   }

   TEST_F(PDFTest, PrintTitleOnPage)
   {
      PDF::PDFError error;

      this->pdf = std::make_shared<PDF>("test",
                                        PDF::File_Operation::WRITE);

      error = this->pdf->setDocumentObjectAttributes(HPDF_COMP_ALL,
                                                     HPDF_PAGE_MODE_USE_OUTLINE);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      error = this->pdf->createNewPage(HPDF_PAGE_SIZE_A4,
                                       HPDF_PAGE_PORTRAIT);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      error = this->pdf->printTitle(0,
                                    "Agres",
                                    "Helvetica",
                                    24);

      ASSERT_EQ(error,
                PDF::PDFError::PDFError_OK);

      return;
   }
}
