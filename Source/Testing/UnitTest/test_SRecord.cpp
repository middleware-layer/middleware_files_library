/**
 ******************************************************************************
 * @file    SRecord_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    15 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/MotorolaSRecorder/SRecord.hpp>

namespace Files
{
   class SRecordTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<SRecord> sRecord;

      public:
         /**
          * @brief	This method will run always before each test
          */
         void SetUp(void)
		 {

		 }

         /**
		   * @brief	This method will run always after each test
		   */
         void TearDown(void)
         {

         }
   };

   TEST_F(SRecordTest, CheckClassInitialization)
   {
      this->sRecord = std::make_shared<SRecord>("test",
                                                SRecord::File_Operation::WRITE);

      ASSERT_NE(this->sRecord->getFile(),
                nullptr);
      return;
   }

   TEST_F(SRecordTest, CheckSharedPointerReset)
   {
      this->sRecord = std::make_shared<SRecord>("test",
                                                SRecord::File_Operation::WRITE);

      this->sRecord.reset();

      ASSERT_EQ(this->sRecord,
                nullptr);

      return;
   }

   TEST_F(SRecordTest, WriteRecordToFile)
   {
      SRecord::SRecordErrors error = SRecord::SRecordErrors::SRECORD_OK;

      this->sRecord = std::make_shared<SRecord>("test",
                                                SRecord::File_Operation::WRITE);

      uint8_t buffer[5] = "";

      for(uint32_t i = 0; i < 20; i++)
      {
         error = this->sRecord->newRecord(SRecord::SRecordTypes::SRECORD_TYPE_S0,
                                           0,
                                           buffer,
                                           5);

         ASSERT_EQ(error,
                   SRecord::SRecordErrors::SRECORD_OK);

         this->sRecord->writeRecord();

         ASSERT_EQ(error,
                   SRecord::SRecordErrors::SRECORD_OK);
      }

      return;
   }

   TEST_F(SRecordTest, ReadIntelHexRecord)
   {
      SRecord::SRecordErrors error = SRecord::SRecordErrors::SRECORD_OK;

      this->sRecord = std::make_shared<SRecord>("test",
                                                SRecord::File_Operation::READ);

      do
      {
         ASSERT_EQ(error,
                   SRecord::SRecordErrors::SRECORD_OK);

         error = this->sRecord->readRecord();

         //this->sRecord->printRecord();

         //printf("\n");
      } while(error == SRecord::SRecordErrors::SRECORD_OK);

      return;
   }
}
