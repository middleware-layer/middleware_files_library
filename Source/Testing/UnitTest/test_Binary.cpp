/**
 ******************************************************************************
 * @file    Binary_test.cpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <gtest/gtest.h>

#include <Production/Binary/Binary.hpp>

namespace Files
{
   class BinaryTest : public ::testing::Test
   {
      private:

      protected:
         std::shared_ptr<Binary> binary;

      public:
         /**
		   * @brief	This method will run always before each test
		   */
		 void SetUp(void)
		 {

		 }

		 /**
		  * @brief	This method will run always after each test
		  */
		 void TearDown(void)
		 {

		 }
   };

   TEST_F(BinaryTest, CheckClassInitialization)
   {
      this->binary = std::make_shared<Binary>("test",
                                              Binary::File_Operation::WRITE);

      ASSERT_NE(this->binary->getFile(),
                nullptr);

      return;
   }

   TEST_F(BinaryTest, CheckSharedPointerReset)
   {
      this->binary = std::make_shared<Binary>("test",
                                              Binary::File_Operation::WRITE);

      this->binary.reset();

      ASSERT_EQ(this->binary,
                nullptr);

      return;
   }
}
