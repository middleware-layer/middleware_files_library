/**
 ******************************************************************************
 * @file    IsoXML_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_ISOXML_ISOXML_STUB_HPP
#define SOURCE_TESTING_STUBS_ISOXML_ISOXML_STUB_HPP

#include <Production/IsoXML/IsoXML.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Files
{
   class IsoXML_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_IsoXML
   {
      public:
         virtual ~I_IsoXML();
   };

   class _Mock_IsoXML : public I_IsoXML
   {
      public:
         _Mock_IsoXML();
   };

   typedef ::testing::NiceMock<_Mock_IsoXML> Mock_IsoXML;

   void stub_setImpl(I_IsoXML* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_ISOXML_ISOXML_STUB_HPP
