/**
 ******************************************************************************
 * @file    IsoXML_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/IsoXML/IsoXML_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Files
{
    static I_IsoXML* p_IsoXML_Impl = NULL;

    const char* IsoXML_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle IsoXML functions";
    }

    void stub_setImpl(I_IsoXML* pNewImpl)
    {
        p_IsoXML_Impl = pNewImpl;
    }

    static I_IsoXML* IsoXML_Stub_Get_Impl(void)
    {
        if(p_IsoXML_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to IsoXML functions. Did you forget" << "to call Stubs::Files::stub_setImpl() ?"
                    << std::endl;

            throw IsoXML_StubImplNotSetException();
        }

        return p_IsoXML_Impl;
    }

    /////////////////////////////////
    // I_IsoXML Methods Definition //
    /////////////////////////////////

    I_IsoXML::~I_IsoXML()
    {
        if(p_IsoXML_Impl == this)
        {
            p_IsoXML_Impl = NULL;
        }
    }

    /////////////////////////////////////
    // _Mock_IsoXML Methods Definition //
    /////////////////////////////////////

    _Mock_IsoXML::_Mock_IsoXML()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    IsoXML::IsoXML(const std::string& _filename, int _flags) :
                    XML(_filename, _flags)
    {
    }

    IsoXML::~IsoXML()
    {
    }
}
