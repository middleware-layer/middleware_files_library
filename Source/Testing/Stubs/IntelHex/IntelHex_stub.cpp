/**
 ******************************************************************************
 * @file    IntelHex_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/IntelHex/IntelHex_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Files
{
    static I_IntelHex* p_IntelHex_Impl = NULL;

    const char* IntelHex_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle IntelHex functions";
    }

    void stub_setImpl(I_IntelHex* pNewImpl)
    {
        p_IntelHex_Impl = pNewImpl;
    }

    static I_IntelHex* IntelHex_Stub_Get_Impl(void)
    {
        if(p_IntelHex_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to IntelHex functions. Did you forget" << "to call Stubs::Simulation::stub_setImpl() ?"
                    << std::endl;

            throw IntelHex_StubImplNotSetException();
        }

        return p_IntelHex_Impl;
    }

    ///////////////////////////////////
    // I_IntelHex Methods Definition //
    ///////////////////////////////////

    I_IntelHex::~I_IntelHex()
    {
        if(p_IntelHex_Impl == this)
        {
            p_IntelHex_Impl = NULL;
        }
    }

    ///////////////////////////////////////
    // _Mock_IntelHex Methods Definition //
    ///////////////////////////////////////

    _Mock_IntelHex::_Mock_IntelHex()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    IntelHex::IntelHex(const std::string& _filename, int _flags)
    {
        mbed::File(this->fileSystem.get(), (_filename + ".hex").c_str(), _flags);
    }

    IntelHex::~IntelHex()
    {
    }

    IntelHex::IHexErrors IntelHex::newRecord(IntelHex::RecordTypes type, uint32_t address, const uint8_t* data,
            uint8_t dataLen)
    {
        return IntelHex_Stub_Get_Impl()->newRecord(type, address, data, dataLen);
    }

    IntelHex::IHexErrors IntelHex::readRecord(void)
    {
        return IntelHex_Stub_Get_Impl()->readRecord();
    }

    IntelHex::IHexErrors IntelHex::writeRecord(void)
    {
        return IntelHex_Stub_Get_Impl()->writeRecord();
    }

    uint8_t IntelHex::Checksum_IHexRecord(void)
    {
        return IntelHex_Stub_Get_Impl()->Checksum_IHexRecord();
    }
}

