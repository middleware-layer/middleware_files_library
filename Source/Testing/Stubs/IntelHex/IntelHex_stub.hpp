/**
 ******************************************************************************
 * @file    IntelHex_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_INTELHEX_INTELHEX_STUB_HPP
#define SOURCE_TESTING_STUBS_INTELHEX_INTELHEX_STUB_HPP

#include <Production/IntelHex/IntelHex.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Files
{
   class IntelHex_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_IntelHex
   {
      public:
         virtual ~I_IntelHex();

         virtual IntelHex::IHexErrors newRecord(      IntelHex::RecordTypes   type,
                                                      uint32_t                address,
                                                const uint8_t*                data,
                                                      uint8_t                 dataLen) = 0;

         virtual IntelHex::IHexErrors readRecord(void) = 0;

         virtual IntelHex::IHexErrors writeRecord(void) = 0;

         virtual uint8_t Checksum_IHexRecord(void) = 0;
   };

   class _Mock_IntelHex : public I_IntelHex
   {
      public:
         _Mock_IntelHex();

         MOCK_METHOD4(newRecord,            IntelHex::IHexErrors    (IntelHex::RecordTypes, uint32_t, const uint8_t*, uint8_t));
         MOCK_METHOD0(readRecord,           IntelHex::IHexErrors    ());
         MOCK_METHOD0(writeRecord,          IntelHex::IHexErrors    ());
         MOCK_METHOD0(Checksum_IHexRecord,  uint8_t                 ());
   };

   typedef ::testing::NiceMock<_Mock_IntelHex> Mock_IntelHex;

   void stub_setImpl(I_IntelHex* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_INTELHEX_INTELHEX_STUB_HPP
