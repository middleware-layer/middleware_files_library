/**
 ******************************************************************************
 * @file    Binary_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Binary/Binary_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Files
{
    static I_Binary* p_Binary_Impl = NULL;

    const char* Binary_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle Binary functions";
    }

    void stub_setImpl(I_Binary* pNewImpl)
    {
        p_Binary_Impl = pNewImpl;
    }

    static I_Binary* Binary_Stub_Get_Impl(void)
    {
        if(p_Binary_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to Binary functions. Did you forget" << "to call Stubs::Files::stub_setImpl() ?"
                    << std::endl;

            throw Binary_StubImplNotSetException();
        }

        return p_Binary_Impl;
    }

    /////////////////////////////////
    // I_Binary Methods Definition //
    /////////////////////////////////

    I_Binary::~I_Binary()
    {
        if(p_Binary_Impl == this)
        {
            p_Binary_Impl = NULL;
        }
    }

    /////////////////////////////////////
    // _Mock_Binary Methods Definition //
    /////////////////////////////////////

    _Mock_Binary::_Mock_Binary()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    Binary::Binary(const std::string& _filename, int _flags)
    {
        mbed::File(this->fileSystem.get(), (_filename + ".b").c_str(), _flags);
    }

    Binary::~Binary()
    {
    }
}
