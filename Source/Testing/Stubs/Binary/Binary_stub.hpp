/**
 ******************************************************************************
 * @file    Binary_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_BINARY_BINARY_STUB_HPP
#define SOURCE_TESTING_STUBS_BINARY_BINARY_STUB_HPP

#include <Production/Binary/Binary.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Files
{
   class Binary_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_Binary
   {
      public:
         virtual ~I_Binary();
   };

   class _Mock_Binary : public I_Binary
   {
      public:
         _Mock_Binary();
   };

   typedef ::testing::NiceMock<_Mock_Binary> Mock_Binary;

   void stub_setImpl(I_Binary* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_BINARY_BINARY_STUB_HPP
