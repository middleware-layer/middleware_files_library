/**
 ******************************************************************************
 * @file    JSon_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_JSON_JSON_STUB_HPP
#define SOURCE_TESTING_STUBS_JSON_JSON_STUB_HPP

#include <Production/Json/JSon.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Files
{
   class JSon_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_JSon
   {
      public:
         virtual ~I_JSon();

         virtual uint32_t getObjectSize(std::string _label) = 0;

         virtual JSon::JsonError clearStructure(void) = 0;

         virtual JSon::JsonError readFile(void) = 0;

         virtual JSon::JsonError writeFile(void) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          const char* _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          bool _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          float _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          double _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          int32_t _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          uint32_t _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<const char*> _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<bool> _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<float> _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<double> _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<int32_t> _value) = 0;

         virtual JSon::JsonError addEntry(std::vector<std::string> _label,
                                          std::initializer_list<uint32_t> _value) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          int32_t& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          bool& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          uint32_t& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::string& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          double& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          float& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<int32_t>& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<bool>& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<uint32_t>& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<std::string>& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<double>& _entry) = 0;

         virtual JSon::JsonError getEntry(std::vector<std::string> _label,
                                          std::vector<float>& _entry) = 0;

         virtual JSon::JsonError deleteEntry(std::vector<std::string> _label) = 0;
   };

   class _Mock_JSon : public I_JSon
   {
      public:
         _Mock_JSon();

         MOCK_METHOD1(getObjectSize, uint32_t (std::string));
         MOCK_METHOD0(clearStructure, JSon::JsonError ());
         MOCK_METHOD0(readFile, JSon::JsonError ());
         MOCK_METHOD0(writeFile, JSon::JsonError ());
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, const char*));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, bool));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, float));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, double));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, int32_t));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, uint32_t));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<const char*>));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<bool>));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<float>));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<double>));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<int32_t>));
         MOCK_METHOD2(addEntry, JSon::JsonError (std::vector<std::string>, std::initializer_list<uint32_t>));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, int32_t&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, bool&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, uint32_t&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::string&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, double&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, float&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<int32_t>&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<bool>&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<uint32_t>&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<std::string>&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<double>&));
         MOCK_METHOD2(getEntry, JSon::JsonError (std::vector<std::string>, std::vector<float>&));
         MOCK_METHOD1(deleteEntry, JSon::JsonError (std::vector<std::string>));
   };

   typedef ::testing::NiceMock<_Mock_JSon> Mock_JSon;

   void stub_setImpl(I_JSon* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_JSON_JSON_STUB_HPP
