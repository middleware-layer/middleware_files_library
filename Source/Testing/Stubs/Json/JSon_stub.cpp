/**
 ******************************************************************************
 * @file    JSon_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Json/JSon_stub.hpp>

#include <Production/Json/JSon.tpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;
using ::testing::Matcher;

//////////////////////
// Mocks themselves //
//////////////////////

namespace Files
{
    static I_JSon* p_JSon_Impl = NULL;

    const char* JSon_StubImplNotSetException::what(void) const throw ()
    {
        return "No stub implementation is set to handle JSon functions";
    }

    void stub_setImpl(I_JSon* pNewImpl)
    {
        p_JSon_Impl = pNewImpl;
    }

    static I_JSon* JSon_Stub_Get_Impl(void)
    {
        if(p_JSon_Impl == NULL)
        {
            std::cerr << "ERROR: No Stub implementation is currently set to handle "
                    << "calls to JSon functions. Did you forget" << "to call Stubs::Simulation::stub_setImpl() ?"
                    << std::endl;

            throw JSon_StubImplNotSetException();
        }

        return p_JSon_Impl;
    }

    ///////////////////////////////
    // I_JSon Methods Definition //
    ///////////////////////////////

    I_JSon::~I_JSon()
    {
        if(p_JSon_Impl == this)
        {
            p_JSon_Impl = NULL;
        }
    }

    ///////////////////////////////////
    // _Mock_JSon Methods Definition //
    ///////////////////////////////////

    _Mock_JSon::_Mock_JSon()
    {
    }

    //////////////////////////////////
    // Definition of Non Stub Class //
    //////////////////////////////////

    JSon::JSon(const std::string& _filename, int _flags)
    {
        mbed::File(this->fileSystem.get(), (_filename + ".json").c_str(), _flags);
    }

    JSon::~JSon()
    {
    }

    uint32_t JSon::getObjectSize(std::string _label)
    {
        return JSon_Stub_Get_Impl()->getObjectSize(_label);
    }

    JSon::JsonError JSon::clearStructure(void)
    {
        return JSon_Stub_Get_Impl()->clearStructure();
    }

    JSon::JsonError JSon::readFile(void)
    {
        return JSon_Stub_Get_Impl()->readFile();
    }

    JSon::JsonError JSon::writeFile(void)
    {
        return JSon_Stub_Get_Impl()->writeFile();
    }

    template<typename TYPE>
    JSon::JsonError JSon::addEntry(std::vector<std::string> _label, TYPE _value)
    {
        return JSon_Stub_Get_Impl()->addEntry(_label, _value);
    }

    template<typename TYPE>
    JSon::JsonError JSon::addEntry(std::vector<std::string> _label, std::initializer_list<TYPE> _value)
    {
        return JSon_Stub_Get_Impl()->addEntry(_label, _value);
    }

    template<typename TYPE>
    JSon::JsonError JSon::getEntry(std::vector<std::string> _label, TYPE& _entry)
    {
        return JSon_Stub_Get_Impl()->getEntry(_label, _entry);
    }

    template<typename TYPE>
    JSon::JsonError JSon::getEntry(std::vector<std::string> _label, std::vector<TYPE>& _entry)
    {
        return JSon_Stub_Get_Impl()->getEntry(_label, _entry);
    }

    JSon::JsonError JSon::deleteEntry(std::vector<std::string> _label)
    {
        return JSon_Stub_Get_Impl()->deleteEntry(_label);
    }
}
