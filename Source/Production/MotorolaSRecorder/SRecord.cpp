/**
 ******************************************************************************
 * @file    SRecord.cpp
 * @author  leonardo
 * @version v1.0
 * @date    15 de jun de 2018
 * @brief   Utility functions to create, read, write, and print Motorola
 *          S-Record binary records.
 ******************************************************************************
 */

#include <Production/MotorolaSRecorder/SRecord.hpp>

#include <stdlib.h>
#include <string.h>

namespace Files
{
    /**
     * @var     SRecord_Address_Lengths
     * @brief   Lengths of the ASCII hex encoded address fields of different SRecord types
     */
    static int SRecord_Address_Lengths[] = { 4, /**< S0 */
                                             4, /**< S1 */
                                             6, /**< S2 */
                                             8, /**< S3 */
                                             8, /**< S4 */
                                             4, /**< S5 */
                                             6, /**< S6 */
                                             8, /**< S7 */
                                             6, /**< S8 */
                                             4, /**< S9 */
    };

    SRecord::SRecord(std::string _filename, int _flags) :
            address(0), dataLen(0), type(
                    SRecordTypes::SRECORD_TYPE_S0), checksum(0)
    {
        this->fileSystem = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".srec").c_str(), _flags);

        this->data.empty();
    }

    SRecord::~SRecord()
    {
    }

    auto SRecord::newRecord(SRecordTypes _type, uint32_t _address, const uint8_t* _data,
            uint32_t _dataLen) -> SRecordErrors
    {
        SRecordErrors retVal = SRecordErrors::SRECORD_OK;

        // Data length size check, assertion of srec pointer
        if((_dataLen < 0) || (_dataLen > (SRecordDefinitions::SRECORD_MAX_DATA_LEN / 2)))
        {
            retVal = SRecordErrors::SRECORD_ERROR_INVALID_ARGUMENTS;
        }

        if(retVal == SRecordErrors::SRECORD_OK)
        {
            this->type = _type;

            this->address = _address;

            memcpy(&this->data.at(0), _data, _dataLen);

            this->dataLen = _dataLen;

            this->checksum = this->Checksum_SRecord();
        }

        return retVal;
    }

    auto SRecord::readRecord(void) -> SRecordErrors
    {
        //*************************//
        // VARIABLE INITIALIZATION //
        //*************************//

        char recordBuff[SRecordDefinitions::SRECORD_RECORD_BUFF_SIZE];

        char hexBuff[SRecordDefinitions::SRECORD_MAX_ADDRESS_LEN + 1]; // A temporary buffer to hold ASCII hex encoded data, set to the maximum length we would ever need

        uint32_t asciiAddressLen;
        uint32_t asciiDataLen;
        uint32_t dataOffset;
        uint32_t fieldDataCount;

        SRecordErrors error;

        //*******************************************************//
        // NEXT WE DO SEVERAL CHECKS FOR THE DESIRED FILE / DATA //
        //*******************************************************//

        // Check our record pointer and file pointer
        if(this->fileSystem == nullptr)
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_ARGUMENTS;
        }

        error = this->getRecordFromFile(recordBuff);

        if(error != SRecordErrors::SRECORD_OK)
        {
            return error;
        }

        // Null-terminate the string at the first sign of a \r or \n
        for(uint32_t i = 0; i < (uint32_t) strlen(recordBuff); i++)
        {
            if(recordBuff[i] == '\r' || recordBuff[i] == '\n')
            {
                recordBuff[i] = 0;

                break;
            }
        }

        // Check if we hit a newline
        if(strlen(recordBuff) == 0)
        {
            return SRecordErrors::SRECORD_ERROR_NEWLINE;
        }

        // Size check for type and count fields
        if(strlen(recordBuff) < (SRecordDefinitions::SRECORD_TYPE_LEN + SRecordDefinitions::SRECORD_COUNT_LEN))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        // Check for the S-Record start code at the beginning of every record
        if(recordBuff[SRecordDefinitions::SRECORD_START_CODE_OFFSET] != SRecordDefinitions::SRECORD_START_CODE)
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        this->copyRecordTypeIntoBuffer(hexBuff, recordBuff + SRecordDefinitions::SRECORD_TYPE_OFFSET);

        this->copyCountFieldIntoBuffer(hexBuff, recordBuff + SRecordDefinitions::SRECORD_COUNT_OFFSET, fieldDataCount);

        // Check that our S-Record type is valid
        if((this->type < SRECORD_TYPE_S0) || (this->type > SRECORD_TYPE_S9))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        // Get the ASCII hex address length of this particular S-Record type
        asciiAddressLen = SRecord_Address_Lengths[this->type];

        // Size check for address field
        if(strlen(recordBuff) < (unsigned int) (SRecordDefinitions::SRECORD_ADDRESS_OFFSET + asciiAddressLen))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        this->copyRecordAddressIntoBuffer(hexBuff, recordBuff + SRecordDefinitions::SRECORD_ADDRESS_OFFSET,
                asciiAddressLen);

        //************************************************************************************************//
        // Compute the ASCII hex data length by subtracting the remaining field lengths from the S-Record //
        // count field (times 2 to account for the number of characters used in ASCII hex encoding)       //
        //************************************************************************************************//

        asciiDataLen = ((fieldDataCount * 2) - asciiAddressLen - SRecordDefinitions::SRECORD_CHECKSUM_LEN);

        // Bailout if we get an invalid data length
        if((asciiDataLen < 0) || (asciiDataLen > SRecordDefinitions::SRECORD_MAX_DATA_LEN))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        // Size check for final data field and checksum field
        if(strlen(recordBuff) < (unsigned int) (SRecordDefinitions::SRECORD_ADDRESS_OFFSET + asciiAddressLen + asciiDataLen + SRecordDefinitions::SRECORD_CHECKSUM_LEN))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        dataOffset = (SRecordDefinitions::SRECORD_ADDRESS_OFFSET + asciiAddressLen);

        // Loop through each ASCII hex byte of the data field, pull it out into hexBuff, convert it and store the result in the data buffer of the S-Record
        for(uint32_t i = 0; i < asciiDataLen / 2; i++)
        {
            this->copyRecordDataIntoBuffer(hexBuff, recordBuff + dataOffset + 2 * i, i);
        }

        // Real data len is divided by two because every byte is represented by two ASCII hex characters
        this->dataLen = (asciiDataLen / 2);

        this->copyRecordCheckSumIntoBuffer(hexBuff, recordBuff + dataOffset + asciiDataLen);

        if(this->checksum != Checksum_SRecord())
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::writeRecord(void) -> SRecordErrors
    {
        char tmpBuffer[SRecordDefinitions::SRECORD_MAX_ADDRESS_LEN + 1];

        uint32_t asciiAddressLen;
        uint32_t asciiAddressOffset;
        uint32_t fieldDataCount;

        // Check our record pointer and file pointer
        if(this->fileSystem == nullptr)
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_ARGUMENTS;
        }

        // Check that the type and data length is within range
        if((this->type < SRecordTypes::SRECORD_TYPE_S0) ||
           (this->type > SRecordTypes::SRECORD_TYPE_S9) ||
           (this->dataLen > SRecordDefinitions::SRECORD_MAX_DATA_LEN / 2))
        {
            return SRecordErrors::SRECORD_ERROR_INVALID_RECORD;
        }

        // Compute the record count, address and checksum lengths are halved because record count is the number of bytes left in the record, not the length of the ASCII hex representation
        fieldDataCount = (SRecord_Address_Lengths[this->type] / 2) + this->dataLen + (SRecordDefinitions::SRECORD_CHECKSUM_LEN / 2);

        asciiAddressLen = SRecord_Address_Lengths[this->type];

        // The offset of the ASCII hex encoded address from zero, this is used so we only write as many bytes of the address as permitted by the S-Record type.
        asciiAddressOffset = (SRecordDefinitions::SRECORD_MAX_ADDRESS_LEN - asciiAddressLen);

        // Lets clean the buffer to not worry about garbage values
        memset(tmpBuffer, 0, sizeof(tmpBuffer));
        snprintf(tmpBuffer, SRecordDefinitions::SRECORD_START_CODE_TYPE_COUNTER_BUFF_SIZE, "%c%1.1X%2.2X", SRecordDefinitions::SRECORD_START_CODE, this->type, fieldDataCount);

        if(this->write(tmpBuffer, SRecordDefinitions::SRECORD_START_CODE_TYPE_COUNTER_BUFF_SIZE) < 0)
        {
            return SRecordErrors::SRECORD_ERROR_FILE;
        }

        //*************************************************************************************//
        // Write the ASCII hex representation of the address, starting from the offset to only //
        // write as much of the address as permitted by the S-Record type (calculated above).  //
        // Fix the hex to be 8 hex digits of precision, to fit all 32-bits, including zeros    //
        //*************************************************************************************//

        // Lets clean the buffer to not worry about garbage values
        memset(tmpBuffer, 0, sizeof(tmpBuffer));
        snprintf(tmpBuffer, SRecordDefinitions::SRECORD_MAX_ADDRESS_LEN, "%2.8X", this->address);

        if(this->write(tmpBuffer + asciiAddressOffset, SRecordDefinitions::SRECORD_MAX_ADDRESS_LEN) < 0)
        {
            return SRecordErrors::SRECORD_ERROR_FILE;
        }

        //***********************************************************************************//
        // Write each byte of the data, guaranteed to be two hex ASCII characters each since //
        // srec->data[i] has the type of uint8_t                                             //
        //***********************************************************************************//

        for(uint32_t i = 0; i < this->dataLen; i++)
        {
            // Lets clean the buffer to not worry about garbage values
            memset(tmpBuffer, 0, sizeof(tmpBuffer));
            snprintf(tmpBuffer, SRecordDefinitions::SRECORD_ASCII_HEX_BYTE_LEN, "%2.2X", this->data[i]);

            if(this->write(tmpBuffer, SRecordDefinitions::SRECORD_ASCII_HEX_BYTE_LEN) < 0)
            {
                return SRecordErrors::SRECORD_ERROR_FILE;
            }
        }

        // Last but not least, the checksum
        uint8_t lengthCheckSumPlusEOL = SRecordDefinitions::SRECORD_CHECKSUM_LEN + 2;

        // Lets clean the buffer to not worry about garbage values
        memset(tmpBuffer, 0, sizeof(tmpBuffer));
        snprintf(tmpBuffer, lengthCheckSumPlusEOL, "%2.2X\r\n", this->Checksum_SRecord());

        if(this->write(tmpBuffer, lengthCheckSumPlusEOL) < 0)
        {
            return SRecordErrors::SRECORD_ERROR_FILE;
        }

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::Checksum_SRecord(void) -> uint8_t
    {
        uint8_t checksum;

        uint32_t fieldDataCount;

        // Compute the record count, address and checksum lengths are halved because record count is the number of bytes left in the record, not the length of the ASCII hex representation
        fieldDataCount = ((SRecord_Address_Lengths[this->type] / 2) + this->dataLen + (SRecordDefinitions::SRECORD_CHECKSUM_LEN / 2));

        // Add the count, address, and data fields together
        checksum = (uint8_t) fieldDataCount;

        // Add each byte of the address individually
        checksum += (uint8_t) (this->address & 0x000000FF);
        checksum += (uint8_t) ((this->address & 0x0000FF00) >> 8);
        checksum += (uint8_t) ((this->address & 0x00FF0000) >> 16);
        checksum += (uint8_t) ((this->address & 0xFF000000) >> 24);

        for(uint32_t i = 0; i < this->dataLen; i++)
        {
            checksum += (uint8_t) this->data.at(i);
        }

        // One's complement the checksum
        checksum = ~checksum;

        return checksum;
    }

    auto SRecord::getRecordFromFile(char* _recordBuff) -> SRecordErrors
    {
        ssize_t bytesRead = this->read(_recordBuff, SRecordDefinitions::SRECORD_RECORD_BUFF_SIZE);

        if(bytesRead <= 0)
        {
            // 0 in case we hit EOF
            if(bytesRead == 0)
            {
                return SRecordErrors::SRECORD_ERROR_EOF;
            }
            else
            {
                return SRecordErrors::SRECORD_ERROR_FILE;
            }
        }

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::copyCountFieldIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t& _dataCount) -> SRecordErrors
    {
        // Copy the ASCII hex encoding of the count field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, SRecordDefinitions::SRECORD_COUNT_LEN);

        _hexBuff[SRecordDefinitions::SRECORD_COUNT_LEN] = 0;

        _dataCount = (int) strtoul(_hexBuff, (char **) NULL, 16);

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::copyRecordAddressIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _address) -> SRecordErrors
    {
        // Copy the ASCII hex encoding of the count field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, (unsigned int) _address);

        _hexBuff[_address] = 0;

        this->address = (uint32_t) strtoul(_hexBuff, (char **) NULL, 16);

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::copyRecordTypeIntoBuffer(char* _hexBuff, char* _recordBuffer) -> SRecordErrors
    {
        // Copy the ASCII hex encoding of the type field into hexBuff, convert it into a usable integer
        strncpy(_hexBuff, _recordBuffer, SRecordDefinitions::SRECORD_TYPE_LEN);

        _hexBuff[SRecordDefinitions::SRECORD_TYPE_LEN] = 0;

        this->type = (int) strtoul(_hexBuff, (char **) NULL, 16);

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::copyRecordCheckSumIntoBuffer(char* _hexBuff, char* _recordBuffer) -> SRecordErrors
    {
        // Copy out the checksum ASCII hex encoded byte, and convert it back to a usable integer
        strncpy(_hexBuff, _recordBuffer, SRecordDefinitions::SRECORD_CHECKSUM_LEN);

        _hexBuff[SRecordDefinitions::SRECORD_CHECKSUM_LEN] = 0;

        this->checksum = (uint8_t) strtoul(_hexBuff, (char **) NULL, 16);

        return SRecordErrors::SRECORD_OK;
    }

    auto SRecord::copyRecordDataIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _offset) -> SRecordErrors
    {
        // Times two i because every byte is represented by two ASCII hex characters
        strncpy(_hexBuff, _recordBuffer, SRecordDefinitions::SRECORD_ASCII_HEX_BYTE_LEN);

        _hexBuff[SRecordDefinitions::SRECORD_ASCII_HEX_BYTE_LEN] = 0;

        this->data.at(_offset) = (uint8_t) strtoul(_hexBuff, (char **) NULL, 16);

        return SRecordErrors::SRECORD_OK;
    }
}
