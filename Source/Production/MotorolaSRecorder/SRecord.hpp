/**
 ******************************************************************************
 * @file    SRecord.hpp
 * @author  leonardo
 * @version v1.0
 * @date    15 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_MOTOROLASRECORDER_SRECORD_HPP
#define SOURCE_PRODUCTION_MOTOROLASRECORDER_SRECORD_HPP

#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>

#include <memory>
#include <array>

namespace Files
{
    /**
     * @class   SRecord
     * @brief   For more information about Intel Hex format file, please access the link below:
     *          https://en.wikipedia.org/wiki/SREC_(file_format)
     *
     *
     */
    class SRecord: public mbed::File
    {
        public:
            /**
             * @enum    SRecordDefinitions
             * @brief   General definition of the S-Record specification
             *
             *
             */
            enum SRecordDefinitions
            {
                SRECORD_RECORD_BUFF_SIZE = 768, /**< 768 should be plenty of space to read in an S-Record */
                SRECORD_TYPE_OFFSET = 1, /**< Offsets and lengths of various fields in an S-Record record */
                SRECORD_TYPE_LEN = 1, /**<  */
                SRECORD_COUNT_OFFSET = 2, /**<  */
                SRECORD_COUNT_LEN = 2, /**<  */
                SRECORD_ADDRESS_OFFSET = 4, /**<  */
                SRECORD_START_CODE_TYPE_COUNTER_BUFF_SIZE = 4, /**<  */
                SRECORD_CHECKSUM_LEN = 2, /**<  */
                SRECORD_MAX_DATA_LEN = 254 * 2, /**< Maximum ascii hex length of the S-Record data field */
                SRECORD_MAX_ADDRESS_LEN = 8, /**< Maximum ascii hex length of the S-Record address field */
                SRECORD_ASCII_HEX_BYTE_LEN = 2, /**< Ascii hex length of a single byte */
                SRECORD_START_CODE_OFFSET = 0, /**< Start code offset and value */
                SRECORD_START_CODE = 'S' /**<  */
            };

            /**
             * @enum    SRecordErrors
             * @brief   All possible error codes the Motorola S-Record utility functions may return.
             *
             *
             */
            enum class SRecordErrors
            {
                SRECORD_OK = 0, /**< Error code for success or no error. */
                SRECORD_ERROR_FILE = -1, /**< Error code for error while reading from or writing to a file. You may check errno for the exact error if this error code is encountered. */
                SRECORD_ERROR_EOF = -2, /**< Error code for encountering end-of-file when reading from a file. */
                SRECORD_ERROR_INVALID_RECORD = -3, /**< Error code for error if an invalid record was read. */
                SRECORD_ERROR_INVALID_ARGUMENTS = -4, /**< Error code for error from invalid arguments passed to function. */
                SRECORD_ERROR_NEWLINE = -5, /**< Error code for encountering a newline with no record when reading from a file. */
            };

            /**
             * @enum    SRecordTypes
             * @brief   Motorola S-Record Types S0-S9
             *
             *
             */
            enum SRecordTypes
            {
                SRECORD_TYPE_S0 = 0, /**< Header record, although there is an official format it is often made proprietary by third-parties. 16-bit address normally set to 0x0000 and header information is stored in the data field. This record is unnecessary and commonly not used. */
                SRECORD_TYPE_S1, /**< Data record with 16-bit address */
                SRECORD_TYPE_S2, /**< Data record with 24-bit address */
                SRECORD_TYPE_S3, /**< Data record with 32-bit address */
                SRECORD_TYPE_S4, /**< Extension by LSI Logic, Inc. See their specification for more details. */
                SRECORD_TYPE_S5, /**< 16-bit address field that contains the number of S1, S2, and S3 (all data) records transmitted. No data field. */
                SRECORD_TYPE_S6, /**< 24-bit address field that contains the number of S1, S2, and S3 (all data) records transmitted. No data field. */
                SRECORD_TYPE_S7, /**< Termination record for S3 data records. 32-bit address field contains address of the entry point after the S-Record file has been processed. No data field. */
                SRECORD_TYPE_S8, /**< Termination record for S2 data records. 24-bit address field contains address of the entry point after the S-Record file has been processed. No data field. */
                SRECORD_TYPE_S9, /**< Termination record for S1 data records. 16-bit address field contains address of the entry point after the S-Record file has been processed. No data field. */
            };

        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

            /**
             * @var     address
             * @brief   The address field. This can be 16, 24, or 32 bits depending on the record type.
             *
             *
             */
            uint32_t address;

            /**
             * @var     data
             * @brief   The 8-bit array data field, which has a maximum size of 32 bytes.
             *
             *
             */
            std::array<uint8_t, SRecordDefinitions::SRECORD_MAX_DATA_LEN / 2> data;

            /**
             * @var     dataLen
             * @brief   The number of bytes of data stored in this record.
             *
             *
             */
            uint32_t dataLen;

            /**
             * @var     type
             * @brief   The Motorola S-Record type of this record (S0-S9).
             *
             *
             */
            uint32_t type;

            /**
             * @var     checksum
             * @brief   The checksum of this record.
             *
             *
             */
            uint8_t checksum;
        protected:
            /**
             * @fn      getRecordFromFile
             * @brief
             *
             *
             * @param   _recordBuff :
             * @return
             */
            auto getRecordFromFile(char* _recordBuff) -> SRecordErrors;

            /**
             * @fn      copyCountFieldIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @param   _dataCount     :
             * @return
             */
            auto copyCountFieldIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t& _dataCount) -> SRecordErrors;

            /**
             * @fn      copyRecordAddressIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordAddressIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _address) -> SRecordErrors;

            /**
             * @fn      copyRecordTypeIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordTypeIntoBuffer(char* _hexBuff, char* _recordBuffer) -> SRecordErrors;

            /**
             * @fn      copyRecordCheckSumIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordCheckSumIntoBuffer(char* _hexBuff, char* _recordBuffer) -> SRecordErrors;

            /**
             * @fn      copyRecordDataIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @param   _offset        :
             * @return
             */
            auto copyRecordDataIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _offset) -> SRecordErrors;

        public:
            /**
             * @fn      SRecord
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags      :
             */
            SRecord(std::string _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~SRecord
             * @brief
             *
             *
             */
            virtual ~SRecord();

            /**
             * @fn      newRecord
             * @brief   Sets all of the record fields of a Motorola S-Record structure.
             *
             *
             * @param   _type      : The Motorola S-Record type (integer value of 0 through 9).
             * @param   _address   : The 32-bit address of the data. The actual size of the address (16-,24-, or 32-bits) when written to a file depends on the S-Record type.
             * @param   _data      : data A pointer to the 8-bit array of data.
             * @param   _dataLen   : The size of the 8-bit data array.
             * @return  SRECORD_OK on success, otherwise one of the SRECORD_ERROR_ error codes.
             * @retval  SRECORD_OK on success.
             * @retval  SRECORD_ERROR_INVALID_ARGUMENTS if the record pointer is NULL, or if the length of the 8-bit data array is out of range (less than zero or greater than the maximum data length allowed by record specifications, see SRecord.data).
             */
            auto newRecord(SRecordTypes _type, uint32_t _address, const uint8_t* _data,
                    uint32_t _dataLen) -> SRecordErrors;

            /**
             * @fn      readRecord
             * @brief   Reads a Motorola S-Record record from an opened file.
             *
             *
             * @return  SRECORD_OK on success, otherwise one of the SRECORD_ERROR_ error codes.
             * @retval  SRECORD_OK on success.
             * @retval  SRECORD_ERROR_INVALID_ARGUMENTS if the record pointer or file pointer is NULL.
             * @retval  SRECORD_ERROR_EOF if end-of-file has been reached.
             * @retval  SRECORD_ERROR_FILE if a file reading error has occured.
             * @retval  SRECORD_INVALID_RECORD if the record read is invalid (record did not match specifications or record checksum was invalid).
             */
            auto readRecord(void) -> SRecordErrors;

            /**
             * @fn      writeRecord
             * @brief   Writes a Motorola S-Record to an opened file.
             *
             *
             * @return  SRECORD_OK on success, otherwise one of the SRECORD_ERROR_ error codes.
             * @retval  SRECORD_OK on success.
             * @retval  SRECORD_ERROR_INVALID_ARGUMENTS if the record pointer or file pointer is NULL.
             * @retval  SRECORD_ERROR_INVALID_RECORD if the record's data length (the SRecord.dataLen variable of the record) is out of range (greater than the maximum data length allowed by record specifications, see SRecord.data).
             * @retval  SRECORD_ERROR_FILE if a file writing error has occured.
             */
            auto writeRecord(void) -> SRecordErrors;

            /**
             * @fn      Checksum_SRecord
             * @brief   Calculates the checksum of a Motorola S-Record SRecord structure.
             *          See the Motorola S-Record specifications for more details on the checksum calculation.
             *
             *
             * @return  The 8-bit checksum.
             */
            auto Checksum_SRecord(void) -> uint8_t;
    };
}

#endif // SOURCE_PRODUCTION_MOTOROLASRECORDER_SRECORD_HPP
