/**
 ******************************************************************************
 * @file    IsoXML.hpp
 * @author  leonardo
 * @version v1.0
 * @date    19 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_ISOXML_ISOXML_HPP
#define SOURCE_PRODUCTION_ISOXML_ISOXML_HPP

#include <Production/XML/XML.hpp>

namespace Files
{
    /**
     * @class   IsoXML
     * @brief   ISO XML is the standard data format for the ISO 11783 (ISOBUS)
     *
     * For more informations about the ISO XML, please read the ISO_11783-10_2009(en).pdf
     * document attached to this project (./Documentation/IsoXML/)
     */
    class IsoXML: public XML
    {
        public:

        private:

        protected:

        public:
            /**
             * @fn      IsoXML
             * @brief
             *
             *
             * @param   _filename  :
             * @param   _flags     :
             */
            IsoXML(const std::string& _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~IsoXML
             * @brief
             *
             *
             */
            virtual ~IsoXML();
    };
}

#endif // SOURCE_PRODUCTION_ISOXML_ISOXML_HPP
