/**
 ******************************************************************************
 * @file    IsoXML.cpp
 * @author  leonardo
 * @version v1.0
 * @date    19 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/IsoXML/IsoXML.hpp>

namespace Files
{
    IsoXML::IsoXML(const std::string& _filename, int _flags) :
            XML(_filename, _flags)
    {

    }

    IsoXML::~IsoXML()
    {

    }
}
