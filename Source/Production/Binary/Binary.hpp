/**
 ******************************************************************************
 * @file    Binary.hpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_BINARY_BINARY_HPP
#define SOURCE_PRODUCTION_BINARY_BINARY_HPP

#include <memory>

#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>

namespace Files
{
    /**
     * @class   Binary
     * @brief
     *
     *
     */
    class Binary: public mbed::File
    {
        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

        protected:

        public:
            /**
             * @fn      Binary
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags      :
             */
            Binary(const std::string& _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~Binary
             * @brief
             *
             *
             */
            virtual ~Binary();
    };
}

#endif // SOURCE_PRODUCTION_BINARY_BINARY_HPP
