/**
 ******************************************************************************
 * @file    Binary.cpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/Binary/Binary.hpp>

namespace Files
{
    Binary::Binary(const std::string& _filename, int _flags)
    {
        this->fileSystem = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".bin").c_str(), _flags);
    }

    Binary::~Binary()
    {

    }
}
