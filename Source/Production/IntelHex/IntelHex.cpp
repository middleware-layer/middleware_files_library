/**
 ******************************************************************************
 * @file    IntelHex.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief   Utility functions to create, read, write, and print Intel
 *          HEX8 binary records.
 ******************************************************************************
 */

#include <stdlib.h>
#include <string.h>

#include <Production/IntelHex/IntelHex.hpp>

namespace Files
{
    IntelHex::IntelHex(const std::string& _filename, int _flags) :
            absoluteRecordAddress(0), recordLength(0), recordType(RecordTypes::IHEX_TYPE_00), recordChecksum(0)
    {
        this->fileSystem = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".srec").c_str(), _flags);

        this->recordData.empty();
    }

    IntelHex::~IntelHex()
    {

    }

    auto IntelHex::newRecord(RecordTypes type, uint32_t address, const uint8_t *data, uint8_t dataLen) -> IHexErrors
    {
        // Data length size check, assertion of ihexRecord pointer
        if((dataLen < 0) || (dataLen > IHexDefinitions::IHEX_MAX_DATA_LEN / 2))
        {
            return IHexErrors::IHEX_ERROR_INVALID_ARGUMENTS;
        }

        this->recordType = type;

        this->absoluteRecordAddress = address;

        memcpy(&this->recordData.at(0), data, dataLen);

        this->recordLength = dataLen;

        this->recordChecksum = Checksum_IHexRecord();

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::readRecord(void) -> IHexErrors
    {
        //*************************//
        // VARIABLE INITIALIZATION //
        //*************************//

        char recordBuff[IHexDefinitions::IHEX_RECORD_BUFF_SIZE];

        char hexBuff[IHexDefinitions::IHEX_ADDRESS_LEN + 1]; // A temporary buffer to hold ASCII hex encoded data, set to the maximum length we would ever need

        uint32_t dataCount;

        IHexErrors error;

        //*******************************************************//
        // NEXT WE DO SEVERAL CHECKS FOR THE DESIRED FILE / DATA //
        //*******************************************************//

        error = this->getRecordFromFile(recordBuff);

        if(error != IHexErrors::IHEX_OK)
        {
            return error;
        }

        // Null-terminate the string at the first sign of a '\r' or '\n'
        for(uint32_t i = 0; i < (uint32_t) strlen(recordBuff); i++)
        {
            if(recordBuff[i] == '\r' || recordBuff[i] == '\n')
            {
                recordBuff[i] = 0;

                break;
            }
        }

        // Check if we hit a newline
        if(strlen(recordBuff) == 0)
        {
            return IHexErrors::IHEX_ERROR_NEWLINE;
        }

        // Size check for start code, count, addess, and type fields
        if(strlen(recordBuff) < (unsigned int) (1 + IHexDefinitions::IHEX_COUNT_LEN + IHexDefinitions::IHEX_ADDRESS_LEN + IHexDefinitions::IHEX_TYPE_LEN))
        {
            return IHexErrors::IHEX_ERROR_INVALID_RECORD;
        }

        // Check for the colon start code
        if(recordBuff[IHexDefinitions::IHEX_START_CODE_OFFSET] != IHexDefinitions::IHEX_START_CODE)
        {
            return IHexErrors::IHEX_ERROR_INVALID_RECORD;
        }

        this->copyCountFieldIntoBuffer(hexBuff, recordBuff + IHexDefinitions::IHEX_COUNT_OFFSET, dataCount);

        this->copyRecordAddressIntoBuffer(hexBuff, recordBuff + IHexDefinitions::IHEX_ADDRESS_OFFSET);

        this->copyRecordTypeIntoBuffer(hexBuff, recordBuff + IHexDefinitions::IHEX_TYPE_OFFSET);

        // Size check for start code, count, address, type, data and checksum fields
        if(strlen(recordBuff)
                < (unsigned int) (1 + IHexDefinitions::IHEX_COUNT_LEN + IHexDefinitions::IHEX_ADDRESS_LEN
                        + IHexDefinitions::IHEX_TYPE_LEN + (dataCount * 2) + IHexDefinitions::IHEX_CHECKSUM_LEN))
        {
            return IHexErrors::IHEX_ERROR_INVALID_RECORD;
        }

        // Loop through each ASCII hex byte of the data field, pull it out into hexBuff, convert it and store the result in the data buffer of the Intel HEX8 record
        for(uint32_t i = 0; i < dataCount; i++)
        {
            this->copyRecordDataIntoBuffer(hexBuff, recordBuff + IHexDefinitions::IHEX_DATA_OFFSET + 2 * i, i);
        }

        this->recordLength = dataCount;

        this->copyRecordCheckSumIntoBuffer(hexBuff, recordBuff + IHexDefinitions::IHEX_DATA_OFFSET + dataCount * 2);

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::writeRecord(void) -> IHexErrors
    {
        char tmpBuffer[IHexDefinitions::IHEX_MAX_TMP_BUFFER_LENGTH + 1];

        // Check our file pointer
        if(this->fileSystem == nullptr)
        {
            return IHexErrors::IHEX_ERROR_INVALID_ARGUMENTS;
        }

        // Check that the data length is in range
        if(this->recordLength > (IHexDefinitions::IHEX_MAX_DATA_LEN / 2))
        {
            return IHexErrors::IHEX_ERROR_INVALID_RECORD;
        }

        memset(tmpBuffer, 0, sizeof(tmpBuffer));
        snprintf(tmpBuffer, sizeof(tmpBuffer), "%c%2.2X%2.4X%2.2X", IHexDefinitions::IHEX_START_CODE, this->recordLength,
                this->absoluteRecordAddress, (uint32_t) this->recordType);

        // Write the start code, data count, address, and type fields
        if(this->write(tmpBuffer, IHexDefinitions::IHEX_MAX_TMP_BUFFER_LENGTH) < 0)
        {
            return IHexErrors::IHEX_ERROR_FILE;
        }

        // Write the data bytes
        for(uint32_t i = 0; i < this->recordLength; i++)
        {
            // Lets clean the buffer to not worry about garbage values
            memset(tmpBuffer, 0, sizeof(tmpBuffer));
            snprintf(tmpBuffer, sizeof(tmpBuffer), "%2.2X", this->recordData.at(i));

            if(this->write(tmpBuffer, IHexDefinitions::IHEX_ASCII_HEX_BYTE_LEN) < 0)
            {
                return IHexErrors::IHEX_ERROR_FILE;
            }
        }

        // Calculate and write the checksum field
        uint8_t lengthCheckSumPlusEOL = IHexDefinitions::IHEX_CHECKSUM_LEN + 2;

        // Lets clean the buffer to not worry about garbage values
        memset(tmpBuffer, 0, sizeof(tmpBuffer));
        snprintf(tmpBuffer, sizeof(tmpBuffer), "%2.2X\r\n", this->Checksum_IHexRecord());

        if(this->write(tmpBuffer, lengthCheckSumPlusEOL) < 0)
        {
            return IHexErrors::IHEX_ERROR_FILE;
        }

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::Checksum_IHexRecord(void) -> uint8_t
    {
        uint8_t checksum;

        // Add the data count, type, address, and data bytes together
        checksum = this->recordLength;
        checksum += (uint8_t) this->recordType;
        checksum += (uint8_t) this->absoluteRecordAddress;
        checksum += (uint8_t) ((this->absoluteRecordAddress & 0xFF00) >> 8);

        for(uint32_t i = 0; i < this->recordLength; i++)
        {
            checksum += (uint8_t) this->recordData.at(i);
        }

        checksum = ~checksum + 1;   // Two's complement on checksum

        return checksum;
    }

    auto IntelHex::getRecordFromFile(char* _recordBuff) -> IHexErrors
    {
        ssize_t bytesRead = this->read(_recordBuff, IHexDefinitions::IHEX_RECORD_BUFF_SIZE);

        if(bytesRead <= 0)
        {
            // In case we hit EOF, don't report a file error
            if(bytesRead == 0)
            {
                return IHexErrors::IHEX_ERROR_EOF;
            }
            else
            {
                return IHexErrors::IHEX_ERROR_FILE;
            }
        }

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::copyCountFieldIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t& _dataCount) -> IHexErrors
    {
        // Copy the ASCII hex encoding of the count field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, IHexDefinitions::IHEX_COUNT_LEN);

        _hexBuff[IHexDefinitions::IHEX_COUNT_LEN] = 0;

        _dataCount = strtoul(_hexBuff, (char **) NULL, 16);

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::copyRecordAddressIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors
    {
        // Copy the ASCII hex encoding of the address field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, IHexDefinitions::IHEX_ADDRESS_LEN);

        _hexBuff[IHexDefinitions::IHEX_ADDRESS_LEN] = 0;

        this->absoluteRecordAddress = (uint32_t) strtoul(_hexBuff, (char **) NULL, 16);

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::copyRecordTypeIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors
    {
        // Copy the ASCII hex encoding of the address field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, IHexDefinitions::IHEX_TYPE_LEN);

        _hexBuff[IHEX_TYPE_LEN] = 0;

        this->recordType = (RecordTypes) strtol(_hexBuff, (char **) NULL, 16);

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::copyRecordCheckSumIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors
    {
        // Copy the ASCII hex encoding of the checksum field into hexBuff, convert it to a usable integer
        strncpy(_hexBuff, _recordBuffer, IHexDefinitions::IHEX_CHECKSUM_LEN);

        _hexBuff[IHexDefinitions::IHEX_CHECKSUM_LEN] = 0;

        this->recordChecksum = (uint8_t) strtol(_hexBuff, (char **) NULL, 16);

        return IHexErrors::IHEX_OK;
    }

    auto IntelHex::copyRecordDataIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _offset) -> IHexErrors
    {
        // Times two i because every byte is represented by two ASCII hex characters
        strncpy(_hexBuff, _recordBuffer, IHexDefinitions::IHEX_ASCII_HEX_BYTE_LEN);

        _hexBuff[IHexDefinitions::IHEX_ASCII_HEX_BYTE_LEN] = 0;

        this->recordData[_offset] = (uint8_t) strtol(_hexBuff, (char **) NULL, 16);

        return IHexErrors::IHEX_OK;
    }
}
