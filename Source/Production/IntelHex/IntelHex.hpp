/**
 ******************************************************************************
 * @file    IntelHex.hpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief   Utility functions to create, read, write, and print Intel
 *          HEX8 binary records.
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_INTELHEX_INTELHEX_HPP
#define SOURCE_PRODUCTION_INTELHEX_INTELHEX_HPP

#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>

#include <stdint.h>
#include <memory>
#include <array>

namespace Files
{
    /**
     * @def     IntelHex
     * @brief   For more information about Intel Hex format file, please access the link below:
     *          https://en.wikipedia.org/wiki/Intel_HEX
     *
     *
     */
    class IntelHex: public mbed::File
    {
        public:
            /**
             * @enum     IHexErrors
             * @brief   All possible error codes the Intel HEX8 record utility functions may return.
             *
             *
             */
            enum class IHexErrors
            {
                IHEX_OK = 0, /**< Error code for success or no error */
                IHEX_ERROR_FILE = -1, /**< Error code for error while reading from or writing to a file. You may check errno for the exact error if this error code is encountered */
                IHEX_ERROR_EOF = -2, /**< Error code for encountering end-of-file when reading from a file */
                IHEX_ERROR_INVALID_RECORD = -3, /**< Error code for error if an invalid record was read */
                IHEX_ERROR_INVALID_ARGUMENTS = -4, /**< Error code for error from invalid arguments passed to function */
                IHEX_ERROR_NEWLINE = -5 /**< Error code for encountering a newline with no record when reading from a file */
            };

            /**
             * @enum    IHexDefinitions
             * @brief   General definition of the Intel HEX8 specification
             *
             *
             */
            enum IHexDefinitions
            {
                IHEX_RECORD_BUFF_SIZE = 768, /**< 768 should be plenty of space to read in a Intel HEX32 record */
                IHEX_COUNT_OFFSET = 1, /**< Offsets and lengths of various fields in an Intel HEX8 record */
                IHEX_COUNT_LEN = 2, /**<  */
                IHEX_ADDRESS_OFFSET = 3, /**<  */
                IHEX_ADDRESS_LEN = 4, /**<  */
                IHEX_TYPE_OFFSET = 7, /**<  */
                IHEX_TYPE_LEN = 2, /**<  */
                IHEX_DATA_OFFSET = 9, /**<  */
                IHEX_CHECKSUM_LEN = 2, /**<  */
                IHEX_MAX_DATA_LEN = 512, /**<  */
                IHEX_ASCII_HEX_BYTE_LEN = 2, /**< Ascii hex encoded length of a single byte */
                IHEX_START_CODE_OFFSET = 0, /**< Start code offset and value */
                IHEX_MAX_TMP_BUFFER_LENGTH = 16, /**< Max Length of temp Buffer */
                IHEX_START_CODE = ':' /**<  */
            };

            /**
             * @enum    RecordTypes
             * @brief   Intel HEX32 Record Types 00-05
             *
             *
             */
            enum class RecordTypes
            {
                IHEX_TYPE_00 = 0, /**< Data Record */
                IHEX_TYPE_01, /**< End of FIL Record */
                IHEX_TYPE_02, /**< Extended Segment Address Record */
                IHEX_TYPE_03, /**< Start Segment Address Record */
                IHEX_TYPE_04, /**< Extended Linear Address Record */
                IHEX_TYPE_05 /**< Start Linear Address Record */
            };

        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

            /**
             * @var     absoluteRecordAddress
             * @brief   The 32-bit address field
             *
             *
             */
            uint32_t absoluteRecordAddress;

            /**
             * @var     recordData
             * @brief   The 8-bit array data field, which has a maximum size of 256 bytes.
             *
             *
             */
            std::array<uint8_t, IHexDefinitions::IHEX_MAX_DATA_LEN> recordData;

            /**
             * @var     recordLength
             * @brief   The number of bytes of data stored in a record
             *
             *
             */
            uint8_t recordLength;

            /**
             * @var     recordType
             * @brief   The Intel HEX8 record type of this record
             *
             *
             */
            RecordTypes recordType;

            /**
             * @var     recordChecksum
             * @brief   The checksum of a record
             *
             *
             */
            uint8_t recordChecksum;

        protected:
            /**
             * @fn      getRecordFromFile
             * @brief
             *
             *
             * @param   _recordBuff :
             * @return
             */
            auto getRecordFromFile(char* _recordBuff) -> IHexErrors;

            /**
             * @fn      copyCountFieldIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff      :
             * @param   _recordBuffer :
             * @param   _dataCount    :
             * @return
             */
            auto copyCountFieldIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t& _dataCount) -> IHexErrors;

            /**
             * @fn      copyRecordAddressIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordAddressIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors;

            /**
             * @fn      copyRecordTypeIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordTypeIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors;

            /**
             * @fn      copyRecordCheckSumIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @return
             */
            auto copyRecordCheckSumIntoBuffer(char* _hexBuff, char* _recordBuffer) -> IHexErrors;

            /**
             * @fn      copyRecordDataIntoBuffer
             * @brief
             *
             *
             * @param   _hexBuff       :
             * @param   _recordBuffer  :
             * @param   _offset        :
             * @return
             */
            auto copyRecordDataIntoBuffer(char* _hexBuff, char* _recordBuffer, uint32_t _offset) -> IHexErrors;

        public:
            /**
             * @fn      IntelHex
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags      :
             */
            IntelHex(const std::string& _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~IntelHex
             * @brief
             *
             *
             */
            virtual ~IntelHex();

            /**
             * @fn      newRecord
             * @brief   Sets all of the record fields of an Intel HEX8 record structure.
             *
             *
             * @param   type        : The Intel HEX8 record type (integer value of 0 through 5).
             * @param   address     : The 32-bit address of the data.
             * @param   data        : A point to the 8-bit array of data.
             * @param   dataLen     : The size of the 8-bit data array.
             * @param   ihexRecord  : A pointer to the target Intel HEX8 record structure where these fields will be set.
             * @return  IHEX_OK on success, otherwise one of the IHEX_ERROR_ error codes.
             * @retval  IHEX_OK on success.
             * @retval  IHEX_ERROR_INVALID_ARGUMENTS if the record pointer is NULL, or if the length of the 8-bit data array is out of range (less than zero or greater than the maximum data length allowed by record specifications, see IHexRecord.data).
             */
            auto newRecord(RecordTypes type, uint32_t address, const uint8_t *data, uint8_t dataLen) -> IHexErrors;

            /**
             * @fn      readRecord
             * @brief   Reads an Intel HEX32 record from an opened file.
             *
             *
             * @retval  IHEX_OK on success, otherwise one of the IHEX_ERROR_ error codes.
             * @retval  IHEX_OK on success.
             * @retval  IHEX_ERROR_INVALID_ARGUMENTS if the record pointer or FIL pointer is NULL.
             * @retval  IHEX_ERROR_EOF if end-of-file has been reached.
             * @retval  IHEX_ERROR_FILE if a FIL reading error has occured.
             * @retval  IHEX_INVALID_RECORD if the record read is invalid (record did not match specifications or record checksum was invalid).
             */
            auto readRecord(void) -> IHexErrors;

            /**
             * @fn      writeRecord
             * @brief   Writes an Intel HEX8 record to an opened file.
             *
             *
             * @return  IHEX_OK on success, otherwise one of the IHEX_ERROR_ error codes.
             * @retval  IHEX_OK on success.
             * @retval  IHEX_ERROR_INVALID_ARGUMENTS if the record pointer or file pointer is NULL.
             * @retval  IHEX_ERROR_INVALID_RECORD if the record's data length is out of range (greater than the maximum data length allowed by record specifications, see IHexRecord.data).
             * @retval  IHEX_ERROR_FILE if a file writing error has occured.
             */
            auto writeRecord(void) -> IHexErrors;

            /**
             * @fn      Checksum_IHexRecord
             * @brief   Calculates the checksum of an Intel HEX8 IHexRecord structure.
             *          See the Intel HEX8 specifications for more details on the checksum calculation.
             *
             *
             * @return  The 8-bit checksum.
             */
            auto Checksum_IHexRecord(void) -> uint8_t;
    };
}

#endif // SOURCE_PRODUCTION_INTELHEX_INTELHEX_HPP
