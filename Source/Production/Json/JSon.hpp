/**
 ******************************************************************************
 * @file    JSon.hpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_JSON_JSON_HPP
#define SOURCE_PRODUCTION_JSON_JSON_HPP

#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>
#include <Production/Json/NLohmann/Json.hpp>

namespace Files
{
    /**
     * @class JSon
     * @brief
     *
     *
     */
    class JSon: public mbed::File
    {
        public:
            /**
             * @enum    JsonError
             * @brief
             *
             * Today, we only support until 3 layers of Objects
             */
            enum class JsonError
            {
                JsonError_OK, /**<  */
                JsonError_PageNotFound, /**<  */
                JsonError_EntryNotFound, /**<  */
                JsonError_WrongSizeRead, /**<  */
                JsonError_WriteOpError /**<  */
            };

            void printToPrompt();

        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

            /**
             * @var     json
             * @brief
             *
             *
             */
            nlohmann::json json;

        protected:
            /**
             * @fn      findEntry
             * @brief
             *
             *
             * @param   _label   :
             * @param   _it      :
             * @return
             */
            auto findEntry(std::vector<std::string> _label, nlohmann::json** _json) -> bool;

            /**
             * @fn      writeToFile
             * @brief
             *
             *
             * @param   _buf    : Buffer to write to file
             * @return
             */
            auto writeToFile(const std::string& _buf) -> JsonError;

            /**
             * @fn      writeObjectOpenToFile
             * @brief
             *
             *
             * @return
             */
            auto writeObjectOpenToFile(void) -> JsonError;

            /**
             * @fn      writeObjectCloseToFile
             * @brief
             *
             *
             * @return
             */
            auto writeObjectCloseToFile(void) -> JsonError;

            /**
             * @fn      writeArrayOpenToFile
             * @brief
             *
             *
             * @return
             */
            auto writeArrayOpenToFile(void) -> JsonError;

            /**
             * @fn      writeObjectCloseToFile
             * @brief
             *
             *
             * @return
             */
            auto writeArrayCloseToFile(void) -> JsonError;

            /**
             * @fn      printObject
             * @brief
             *
             *
             * @param   _object        :
             * @return
             */
            auto printObject(nlohmann::json _object) -> JsonError;

            /**
             * @fn      printArray
             * @brief
             *
             *
             * @param   _object        :
             * @return
             */
            auto printArray(nlohmann::json _array) -> JsonError;

            /**
             * @fn      decodeAndPrintObjectValueType
             * @brief
             *
             *
             * @param   _it   :
             * @return
             */
            auto decodeAndPrintObjectValueType(nlohmann::json::iterator _it) -> JsonError;

        public:
            /**
             * @fn      JSon
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags  :
             */
            JSon(const std::string& _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~JSon
             * @brief
             *
             *
             */
            virtual ~JSon();

            /**
             * @fn      getObjectSize
             * @brief
             *
             *
             * @param   _label   :
             * @return
             */
            auto getObjectSize(std::string _label = "") -> uint32_t;

            /**
             * @fn      isJsonEmpty
             * @brief
             *
             *
             * @return
             */
            auto isJsonEmpty(void) -> bool
            {
                return this->json.empty();
            }

            /**
             * @fn      clearStructure
             * @brief
             *
             *
             * @return
             */
            auto clearStructure(void) -> JsonError;

            /**
             * @fn      readFile
             * @brief
             *
             *
             * @return
             */
            auto readFile(void) -> JsonError;

            /**
             * @fn      writeFile
             * @brief
             *
             *
             * @return
             */
            auto writeFile(void) -> JsonError;

            /**
             * @fn      addEntry
             * @brief
             *
             *
             * @param   _label   :
             * @param   _value   :
             * @return
             * @retval  JsonError_OK on success.
             */
            template<typename TYPE>
            auto addEntry(std::vector<std::string> _label, TYPE _value) -> JsonError;

            /**
             * @fn      addEntry
             * @brief
             *
             *
             * @param   _label   :
             * @param   _value   :
             * @return
             * @retval  JsonError_OK on success.
             */
            template<typename TYPE>
            auto addEntry(std::vector<std::string> _label, std::initializer_list<TYPE> _value) -> JsonError;

            /**
             * @fn      getEntry
             * @brief
             *
             *
             * @param _ label :
             * @return
             */
            template<typename TYPE>
            auto getEntry(std::vector<std::string> _label, TYPE& _entry) -> JsonError;

            /**
             * @fn      getEntry
             * @brief
             *
             *
             * @param _ label :
             * @return
             */
            template<typename TYPE>
            auto getEntry(std::vector<std::string> _label, std::vector<TYPE>& _entry) -> JsonError;
            /**
             * @fn      deleteEntry
             * @brief
             *
             *
             * @param   _label   :
             * @return
             */
            auto deleteEntry(std::vector<std::string> _label) -> JsonError;
    };
}

#endif // SOURCE_PRODUCTION_JSON_JSON_HPP
