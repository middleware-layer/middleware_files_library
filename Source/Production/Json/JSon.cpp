/**
 ******************************************************************************
 * @file    JSon.cpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/Json/JSon.hpp>
#include <Production/Json/JSon.tpp>

#include <iostream>

namespace Files
{
    JSon::JSon(const std::string& _filename, int _flags)
    {
        this->fileSystem = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".srec").c_str(), _flags);
    }

    JSon::~JSon()
    {

    }

    auto JSon::clearStructure(void) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        this->json.clear();

        return error;
    }

    auto JSon::getObjectSize(std::string _label) -> uint32_t
    {
        uint32_t returnValue = 0;

        if(_label.compare("") == 0)
        {
            returnValue = this->json.size();
        }

        else
        {
            returnValue = this->json[_label].size();
        }

        return returnValue;
    }

    auto JSon::writeToFile(const std::string& _buf) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        if(this->write(_buf.c_str(), _buf.length()) < 0)
        {
            error = JsonError::JsonError_WriteOpError;
        }

        return error;
    }

    auto JSon::writeObjectOpenToFile(void) -> JsonError
    {
        return this->writeToFile("{");
    }

    auto JSon::writeObjectCloseToFile(void) -> JsonError
    {
        return this->writeToFile("}");
    }

    auto JSon::writeArrayOpenToFile(void) -> JsonError
    {
        return this->writeToFile("[");
    }

    auto JSon::writeArrayCloseToFile(void) -> JsonError
    {
        return this->writeToFile("]");
    }

    auto JSon::printObject(nlohmann::json _object) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        for(nlohmann::json::iterator it = _object.begin(); it != _object.end(); ++it)
        {
            std::string tmpBuffer = "";

            if(it != _object.begin())
            {
                tmpBuffer += ", ";
            }

            tmpBuffer += '"';
            tmpBuffer += it.key();
            tmpBuffer += '"';

            error = this->writeToFile(tmpBuffer);

            this->decodeAndPrintObjectValueType(it);
        }

        return error;
    }

    auto JSon::printArray(nlohmann::json _array) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        for(nlohmann::json::iterator it = _array.begin(); it != _array.end(); ++it)
        {
            std::string tmpBuffer = "";

            if(it != _array.begin())
            {
                tmpBuffer += ',';
            }

            tmpBuffer += ' ';

            error = this->writeToFile(tmpBuffer);

            this->decodeAndPrintObjectValueType(it);
        }

        return error;
    }

    auto JSon::decodeAndPrintObjectValueType(nlohmann::json::iterator _it) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        std::string tmpBuffer = "";

        if(_it->is_string())
        {
            std::string value = _it.value();

            tmpBuffer += '"';
            tmpBuffer += value;
            tmpBuffer += '"';

            error = this->writeToFile(tmpBuffer);
        }

        else if(_it->is_boolean())
        {
            bool value = (_it.value() == true) ? "true" : "false";

            tmpBuffer += value;

            error = this->writeToFile(tmpBuffer);
        }

        else if(_it->is_number_float())
        {
            float value = _it.value();

            tmpBuffer += value;

            error = this->writeToFile(tmpBuffer);
        }

        else if(_it->is_number_integer())
        {
            int32_t value = _it.value();

            tmpBuffer += value;

            error = this->writeToFile(tmpBuffer);
        }

        else if(_it->is_number_unsigned())
        {
            uint32_t value = _it.value();

            tmpBuffer += value;

            error = this->writeToFile(tmpBuffer);
        }

        else if(_it->is_array())
        {
            this->writeArrayOpenToFile();

            this->printArray(*_it);

            this->writeArrayCloseToFile();
        }

        else if(_it->is_object())
        {
            this->writeObjectOpenToFile();

            this->printObject(*_it);

            this->writeObjectCloseToFile();
        }

        return error;
    }

    auto JSon::readFile(void) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        uint32_t fileSize = this->size();

        std::string buffer;

        buffer.resize(fileSize);

        // copy all the text into the buffer
        if(this->read(&buffer[0], fileSize)!= fileSize)
        {
            error = JsonError::JsonError_WrongSizeRead;
        }

        else
        {
            this->json = nlohmann::json::parse(buffer.c_str(), buffer.c_str() + buffer.size());
        }

        return error;
    }

    auto JSon::writeFile(void) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        this->writeObjectOpenToFile();

        this->printObject(this->json);

        this->writeObjectCloseToFile();

        return error;
    }

    auto JSon::findEntry(std::vector<std::string> _label, nlohmann::json** _json) -> bool
    {
        bool returnValue = false;

        nlohmann::json::iterator it;

        if(_label.size() == 1)
        {
            it = this->json.find(_label.at(0));

            if(it != this->json.end())
            {
                returnValue = true;

                *_json = &this->json.at(it.key());
            }

            else
            {
                returnValue = false;

                *_json = &this->json[_label.at(0)];
            }
        }

        if(_label.size() > 1)
        {
            it = this->json[_label.at(0)].find(_label.at(1));

            if(it != this->json[_label.at(0)].end())
            {
                returnValue = true;

                *_json = &this->json[_label.at(0)].at(it.key());
            }

            else
            {
                returnValue = false;

                *_json = &this->json[_label.at(0)][_label.at(1)];
            }
        }

        if(_label.size() > 2)
        {
            it = this->json[_label.at(0)][_label.at(1)].find(_label.at(2));

            if(it != this->json[_label.at(0)][_label.at(1)].end())
            {
                returnValue = true;

                *_json = &this->json[_label.at(0)][_label.at(1)].at(it.key());
            }

            else
            {
                returnValue = false;

                *_json = &this->json[_label.at(0)][_label.at(1)][_label.at(2)];
            }
        }

        return returnValue;
    }

    template<typename TYPE>
    auto JSon::addEntry(std::vector<std::string> _label, TYPE _value) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        nlohmann::json* jsonPointer;

        if(this->findEntry(_label, &jsonPointer))
        {
            *jsonPointer += _value;
        }

        else
        {
            *jsonPointer = _value;
        }

        return error;
    }

    template<typename TYPE>
    auto JSon::addEntry(std::vector<std::string> _label, std::initializer_list<TYPE> _value) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        nlohmann::json* jsonPointer;

        if(this->findEntry(_label, &jsonPointer))
        {
            for(TYPE iterator : _value)
            {
                this->addEntry(_label, iterator);
            }
        }

        else
        {
            *jsonPointer = _value;
        }

        return error;
    }

    template<typename TYPE>
    auto JSon::getEntry(std::vector<std::string> _label, TYPE& _entry) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        nlohmann::json* jsonPointer;

        if(this->findEntry(_label, &jsonPointer))
        {
            _entry = *jsonPointer;
        }

        else
        {
            error = JsonError::JsonError_EntryNotFound;
        }

        return error;
    }

    template<typename TYPE>
    auto JSon::getEntry(std::vector<std::string> _label, std::vector<TYPE>& _entry) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        nlohmann::json* jsonPointer;

        if(this->findEntry(_label, &jsonPointer))
        {
            for(TYPE iterator : *jsonPointer)
            {
                _entry.push_back(iterator);
            }
        }

        else
        {
            error = JsonError::JsonError_EntryNotFound;
        }

        return error;
    }

    auto JSon::deleteEntry(std::vector<std::string> _label) -> JsonError
    {
        JsonError error = JsonError::JsonError_OK;

        /*
         if(this->findEntry(_label))
         {
         this->json.erase(_label.at(0));   // delete the entry
         }
         */

        return error;
    }

    void JSon::printToPrompt()
    {
        std::cout << std::setw(4) << this->json << std::endl;
    }
}
