/**
 ******************************************************************************
 * @file    JSon.tpp
 * @author  leonardo
 * @version v1.0
 * @date    18 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_JSON_JSON_TPP
#define SOURCE_PRODUCTION_JSON_JSON_TPP

#include <iostream>

namespace Files
{
   template auto JSon::addEntry(std::vector<std::string> _label,
                                const char* _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                bool _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                float _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                double _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                int32_t _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                uint32_t _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<const char*> _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<bool> _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<float> _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<double> _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<int32_t> _value) -> JsonError;

   template auto JSon::addEntry(std::vector<std::string> _label,
                                std::initializer_list<uint32_t> _value) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                int32_t& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                bool& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                uint32_t& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::string& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                double& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                float& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<int32_t>& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<bool>& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<uint32_t>& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<std::string>& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<double>& _entry) -> JsonError;

   template auto JSon::getEntry(std::vector<std::string> _label,
                                std::vector<float>& _entry) -> JsonError;
}

#endif // SOURCE_PRODUCTION_JSON_JSON_TPP
