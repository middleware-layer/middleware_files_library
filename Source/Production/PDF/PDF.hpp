/**
 ******************************************************************************
 * @file    PDF.hpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_PDF_PDF_HPP
#define SOURCE_PRODUCTION_PDF_PDF_HPP

#include <Production/PDF/libHaru/include/hpdf.h>
#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>

#include <vector>
#include <memory>

namespace Files
{
    /**
     * @class   PDF
     * @brief
     *
     *
     */
    class PDF: public mbed::File
    {
        public:
            /**
             * @enum    PDFError
             * @brief
             *
             *
             */
            enum class PDFError
            {
                PDFError_OK, /**<  */
                PDFError_PageNotFound, /**<  */
            };

        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

            /**
             * @var     filename
             * @brief
             *
             *
             */
            std::string filename;

            /**
             * @var     pdfDocument
             * @brief
             *
             *
             */
            HPDF_Doc pdfDocument;

            /**
             * @var     pdfPages
             * @brief
             *
             *
             */
            std::vector<HPDF_Page> pdfPages;

            /**
             * @fn      saveFile
             * @brief
             *
             *
             * @return
             * @retval  PDFError_OK on success.
             */
            auto saveFile(void) -> PDFError;

            /**
             * @fn      getPageAtPosition
             * @brief
             *
             *
             * @param   _pagePosition  :
             * @param   _page          :
             * @return
             * @retval  PDFError_OK on success.
             * @retval  PDFError_PageNotFound if the desired page could not be found
             */
            auto getPageAtPosition(uint16_t _pagePosition, HPDF_Page& _page) -> PDFError;

        protected:

        public:
            /**
             * @fn      PDF
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags      :
             */
            PDF(std::string _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~PDF
             * @brief
             *
             *
             */
            virtual ~PDF();

            /**
             * @fn      setDocumentObjectAttributes
             * @brief
             *
             *
             * @param   _mode             :
             * @param   _pageMode         :
             * @param   _encryptDoc       :
             * @param   _owner_password   :
             * @param   _user_password    :
             * @return
             * @retval  PDFError_OK on success.
             */
            auto setDocumentObjectAttributes(HPDF_UINT _mode, HPDF_PageMode _pageMode, bool _encryptDoc = false,
                    const char* _owner_password = "", const char* _user_password = "") -> PDFError;

            /**
             * @fn      createNewPage
             * @brief
             *
             *
             * @param   _pageSize      :
             * @param   _pageDirection :
             * @return
             * @retval  PDFError_OK on success.
             */
            auto createNewPage(HPDF_PageSizes _pageSize, HPDF_PageDirection _pageDirection) -> PDFError;

            /**
             * @fn      insertNewPageAtPosition
             * @brief
             *
             *
             * @param   _pageSize      :
             * @param   _pageDirection :
             * @return
             * @retval  PDFError_OK on success.
             * @retval  PDFError_PageNotFound if the desired page position could not be found
             */
            auto insertNewPageAtPosition(HPDF_PageSizes _pageSize, HPDF_PageDirection _pageDirection,
                    uint16_t _pagePosition) -> PDFError;

            /**
             * @fn      getPageSize
             * @brief
             *
             *
             * @param   _pagePosition  :
             * @param   _height        :
             * @param   _width         :
             * @return
             * @retval  PDFError_OK on success.
             * @retval  PDFError_PageNotFound if the desired page could not be found
             */
            auto getPageSize(uint16_t _pagePosition, HPDF_REAL& _height, HPDF_REAL& _width) -> PDFError;

            /**
             * @fn      printTitle
             * @brief
             *
             *
             * @param   _pagePosition  :
             * @param   _title         :
             * @param   _fontName      :
             * @param   _fontSize      :
             * @param   _encodingName  :
             * @return
             * @retval  PDFError_OK on success.
             * @retval  PDFError_PageNotFound if the desired page could not be found
             */
            auto printTitle(uint16_t _pagePosition, std::string _title, std::string _fontName, HPDF_REAL _fontSize,
                    std::string _encodingName = "CP1252") -> PDFError;

            /**
             * @fn      printText
             * @brief
             *
             *
             * @param   _page       :
             * @param   _text       :
             * @param   _Xposition  :
             * @param   _Yposition  :
             * @return
             * @retval  PDFError_OK on success.
             * @retval  PDFError_PageNotFound if the desired page could not be found
             */
            auto printText(HPDF_Page _page, std::string _text, HPDF_REAL _Xposition, HPDF_REAL _Yposition) -> PDFError;
    };
}

#endif // SOURCE_PRODUCTION_PDF_PDF_HPP
