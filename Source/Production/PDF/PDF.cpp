/**
 ******************************************************************************
 * @file    PDF.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <iostream>

#include <Production/PDF/PDF.hpp>

namespace Files
{
    PDF::PDF(std::string _filename, int _flags) : filename(filename)
    {
        this->fileSystem = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".pdf").c_str(), _flags);

        this->pdfDocument = HPDF_New(nullptr, NULL);
    }

    PDF::~PDF()
    {
        HPDF_Free(this->pdfDocument);
    }

    auto PDF::saveFile(void) -> PDFError
    {
        HPDF_SaveToFile(this->pdfDocument, this->filename.c_str());

        return PDFError::PDFError_OK;
    }

    auto PDF::setDocumentObjectAttributes(HPDF_UINT _mode, HPDF_PageMode _pageMode, bool _encryptDoc,
            const char* _owner_password, const char* _user_password) -> PDFError
    {
        // set compression mode
        HPDF_SetCompressionMode(this->pdfDocument, _mode);

        // set page mode to use outlines
        HPDF_SetPageMode(this->pdfDocument, _pageMode);

        if(_encryptDoc)
        {
            // set password if wanted
            HPDF_SetPassword(this->pdfDocument, _owner_password, _user_password);
        }

        this->saveFile();

        return PDFError::PDFError_OK;
    }

    auto PDF::getPageAtPosition(uint16_t _pagePosition, HPDF_Page& _page) -> PDFError
    {
        PDFError error = PDFError::PDFError_OK;

        if(((uint16_t) this->pdfPages.size()) >= (_pagePosition + 1))
        {
            _page = this->pdfPages.at(_pagePosition);

            error = PDFError::PDFError_OK;
        }

        else
        {
            error = PDFError::PDFError_PageNotFound;
        }

        return error;
    }

    auto PDF::createNewPage(HPDF_PageSizes _pageSize, HPDF_PageDirection _pageDirection) -> PDFError
    {
        HPDF_Page page = HPDF_AddPage(this->pdfDocument);

        HPDF_Page_SetSize(page, _pageSize, _pageDirection);

        this->pdfPages.push_back(page);

        this->saveFile();

        return PDFError::PDFError_OK;
    }

    auto PDF::insertNewPageAtPosition(HPDF_PageSizes _pageSize, HPDF_PageDirection _pageDirection,
            uint16_t _pagePosition) -> PDFError
    {
        PDFError error = PDFError::PDFError_OK;

        HPDF_Page newPage;
        HPDF_Page oldPage;

        error = this->getPageAtPosition(_pagePosition, oldPage);

        // If the getPageAtPosition() method returned without error
        if(error == PDFError::PDFError_OK)
        {
            newPage = HPDF_InsertPage(this->pdfDocument, oldPage);

            HPDF_Page_SetSize(newPage, _pageSize, _pageDirection);

            this->pdfPages.insert(this->pdfPages.begin() + _pagePosition, newPage);

            this->saveFile();
        }

        return error;
    }

    auto PDF::getPageSize(uint16_t _pagePosition, HPDF_REAL& _height, HPDF_REAL& _width) -> PDFError
    {
        PDFError error = PDFError::PDFError_OK;

        HPDF_Page page;

        error = this->getPageAtPosition(_pagePosition, page);

        // If the getPageAtPosition() method returned without error
        if(error == PDFError::PDFError_OK)
        {
            _height = HPDF_Page_GetHeight(page);
            _width = HPDF_Page_GetWidth(page);
        }

        return error;
    }

    auto PDF::printTitle(uint16_t _pagePosition, std::string _title, std::string _fontName, HPDF_REAL _fontSize,
            std::string _encodingName) -> PDFError
    {
        PDFError error = PDFError::PDFError_OK;

        HPDF_Page page;

        HPDF_REAL height;
        HPDF_REAL width;
        HPDF_REAL textWidth;

        HPDF_Font def_font;

        error = this->getPageAtPosition(_pagePosition, page);

        // If the getPageAtPosition() method returned without error
        if(error == PDFError::PDFError_OK)
        {
            error = this->getPageSize(_pagePosition, height, width);

            if(error == PDFError::PDFError_OK)
            {
                def_font = HPDF_GetFont(this->pdfDocument, _fontName.c_str(), _encodingName.c_str());

                HPDF_Page_SetFontAndSize(page, def_font, _fontSize);

                textWidth = HPDF_Page_TextWidth(page, _title.c_str());

                this->printText(page, _title, (width - textWidth) / 2, height - 50);

                this->saveFile();
            }
        }

        return error;
    }

    auto PDF::printText(HPDF_Page _page, std::string _text, HPDF_REAL _Xposition, HPDF_REAL _Yposition) -> PDFError
    {
        PDFError error = PDFError::PDFError_OK;

        HPDF_Page_BeginText(_page);

        HPDF_Page_TextOut(_page, _Xposition, _Yposition, _text.c_str());

        HPDF_Page_EndText(_page);

        return error;
    }
}
