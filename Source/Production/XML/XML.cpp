/**
 ******************************************************************************
 * @file    XML.cpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#include <Production/XML/XML.hpp>
#include <Production/XML/XML.tpp>

namespace Files
{
    XML::XML(std::string _filename, int _flags) :
            xmlRootNode(nullptr), filename(_filename)
    {
        this->xmlDocument   = std::make_shared<tinyxml2::XMLDocument>();
        this->fileSystem    = std::make_shared<FATFileSystem>();

        mbed::File(this->fileSystem.get(), (_filename + ".xml").c_str(), _flags);
    }

    XML::~XML()
    {
    }

    auto XML::checkError(tinyxml2::XMLError _error) -> XMLError
    {
        XMLError error;

        switch(_error)
        {
            case tinyxml2::XMLError::XML_SUCCESS:
                error = XMLError::OK;
                break;

            case tinyxml2::XMLError::XML_NO_ATTRIBUTE:
                error = XMLError::NO_ATTRIBUTE;
                break;

            case tinyxml2::XMLError::XML_ERROR_FILE_NOT_FOUND:
                error = XMLError::FILE_NOT_FOUND;
                break;

            case tinyxml2::XMLError::XML_ERROR_FILE_COULD_NOT_BE_OPENED:
                error = XMLError::FILE_COULD_NOT_BE_OPENED;
                break;

            case tinyxml2::XMLError::XML_ERROR_FILE_READ_ERROR:
                error = XMLError::FILE_READ_ERROR;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_ELEMENT:
                error = XMLError::ERROR_PARSING_ELEMENT;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_ATTRIBUTE:
                error = XMLError::ERROR_PARSING_ATTRIBUTE;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_TEXT:
                error = XMLError::ERROR_PARSING_TEXT;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_CDATA:
                error = XMLError::ERROR_PARSING_CDATA;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_COMMENT:
                error = XMLError::ERROR_PARSING_COMMENT;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_DECLARATION:
                error = XMLError::ERROR_PARSING_DECLARATION;
                break;

            case tinyxml2::XMLError::XML_ERROR_PARSING_UNKNOWN:
                error = XMLError::ERROR_PARSING_UNKNOWN;
                break;

            case tinyxml2::XMLError::XML_ELEMENT_DEPTH_EXCEEDED:
                error = XMLError::ELEMENT_DEPTH_EXCEEDED;
                break;

            case tinyxml2::XMLError::XML_WRONG_ATTRIBUTE_TYPE:
            case tinyxml2::XMLError::UNUSED_XML_ERROR_ELEMENT_MISMATCH:
            case tinyxml2::XMLError::UNUSED_XML_ERROR_IDENTIFYING_TAG:
            case tinyxml2::XMLError::XML_ERROR_EMPTY_DOCUMENT:
            case tinyxml2::XMLError::XML_ERROR_COUNT:
            case tinyxml2::XMLError::XML_NO_TEXT_NODE:
            case tinyxml2::XMLError::XML_CAN_NOT_CONVERT_TEXT:
            case tinyxml2::XMLError::XML_ERROR_PARSING:
            case tinyxml2::XMLError::XML_ERROR_MISMATCHED_ELEMENT:
            default:
                error = XMLError::UNDEFINED_ERROR;
                break;
        }

        return error;
    }

    auto XML::createElement(std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element = this->xmlDocument->NewElement(_name.c_str());

        if(element == nullptr)
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            this->openedElementsMap[_name] = element;
        }

        return error;
    }

    auto XML::openElement(std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element = this->xmlRootNode->FirstChildElement(_name.c_str());

        if(element == nullptr)
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            this->openedElementsMap[_name] = element;
        }

        return error;
    }

    auto XML::openElement(std::string _fatherElement, std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element;

        std::map<std::string, tinyxml2::XMLElement*>::iterator fatherElement = this->getOpenedElement(_fatherElement);

        if(fatherElement == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            element = fatherElement->second->FirstChildElement(_name.c_str());

            if(element == nullptr)
            {
                error = XMLError::NULL_ELEMENT;
            }

            else
            {
                this->openedElementsMap[_name] = element;
            }
        }

        return error;
    }

    auto XML::closeElement(std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator element = this->getOpenedElement(_name);

        if(element == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            this->xmlRootNode->InsertEndChild(element->second);

            this->openedElementsMap.erase(element);
        }

        return error;
    }

    auto XML::closeElement(std::string _fatherElement, std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator fatherElement = this->getOpenedElement(_fatherElement);
        std::map<std::string, tinyxml2::XMLElement*>::iterator element = this->getOpenedElement(_name);

        if((element == this->openedElementsMap.end()) || (fatherElement == this->openedElementsMap.end()))
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            fatherElement->second->InsertEndChild(element->second);

            this->openedElementsMap.erase(element);
        }

        return error;
    }

    auto XML::eraseElementFromMap(std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator element = this->getOpenedElement(_name);

        if(element == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            this->openedElementsMap.erase(element);
        }

        return error;
    }

    auto XML::getOpenedElement(std::string _name) -> std::map<std::string, tinyxml2::XMLElement*>::iterator
    {
        std::map<std::string, tinyxml2::XMLElement*>::iterator it;

        it = this->openedElementsMap.find(_name);

        return it;
    }

    auto XML::saveFile(bool _compactMode) -> XMLError
    {
        tinyxml2::XMLError result = tinyxml2::XMLError::XML_SUCCESS;

        result = this->xmlDocument->SaveFile(this->filename.c_str(), _compactMode);

        return this->checkError(result);
    }

    //TODO The TinyXML Library works with Linux File System, need to port to FATFs
    auto XML::readFile(void) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLError result = tinyxml2::XMLError::XML_SUCCESS;

        result = this->xmlDocument->LoadFile(this->filename.c_str());

        error = this->checkError(result);

        //****************************************************************************
        // From now on, we set out private pointers to be able to access them later //
        //****************************************************************************

        this->xmlRootNode = this->xmlDocument->FirstChild();

        if(this->xmlRootNode == nullptr)
        {
            error = XMLError::POORLY_CONSTRUCTED_FILE;
        }

        return error;
    }

    auto XML::openXMLPrinter(void) -> XMLError
    {
        XMLError error = XMLError::OK;

//        this->xmlPrinter = std::make_shared<tinyxml2::XMLPrinter>(this->file);

        return error;
    }

    auto XML::setRootNode(std::string _name) -> XMLError
    {
        XMLError error = XMLError::OK;

        this->xmlRootNode = this->xmlDocument->NewElement(_name.c_str());

        this->xmlDocument->InsertFirstChild(this->xmlRootNode);

        return error;
    }

    template<typename TYPE>
    auto XML::writeElement(std::string _name, TYPE _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element = this->xmlDocument->NewElement(_name.c_str());

        element->SetText(_value);

        if(this->xmlRootNode->InsertEndChild(element) == nullptr)
        {
            error = XMLError::UNDEFINED_ERROR;
        }

        return error;
    }

    template<typename TYPE>
    auto XML::writeElement(std::string _fatherName, std::string _name, TYPE _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator fatherElement = this->getOpenedElement(_fatherName);

        tinyxml2::XMLElement* element = this->xmlDocument->NewElement(_name.c_str());

        element->SetText(_value);

        if(fatherElement->second->InsertEndChild(element) == nullptr)
        {
            error = XMLError::UNDEFINED_ERROR;
        }

        return error;
    }

    template<typename TYPE>
    auto XML::writeAttributeToOpenedElement(std::string _fatherElement, std::string _attributeName,
            TYPE _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        if(this->getOpenedElement(_fatherElement) == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            this->openedElementsMap.at(_fatherElement)->SetAttribute(_attributeName.c_str(), _value);
        }

        return error;
    }

    template<typename TYPE>
    auto XML::addChildElementToOpenedElement(std::string _fatherElement, std::string _childElement,
            TYPE _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element;

        if(this->getOpenedElement(_fatherElement) == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            element = this->xmlDocument->NewElement(_childElement.c_str());

            element->SetText(_value);

            if(this->openedElementsMap.at(_fatherElement)->InsertEndChild(element) == nullptr)
            {
                error = XMLError::UNDEFINED_ERROR;
            }
        }

        return error;
    }

    template<typename TYPE>
    auto XML::addChildElementToOpenedElement(std::string _fatherElement, std::string _childElement,
            std::vector<TYPE> _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        if(this->getOpenedElement(_fatherElement) == this->openedElementsMap.end())
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            for(const auto & item : _value)
            {
                error = this->addChildElementToOpenedElement(_fatherElement, _childElement, item);
            }
        }

        return error;
    }

    template<typename TYPE>
    auto XML::queryAttribute(tinyxml2::XMLElement* _element, std::string _name, TYPE* _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        _element->QueryAttribute(_name.c_str(), _value);

        return error;
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, bool* _value) -> XMLError
    {
        return this->checkError(_element->QueryBoolText(_value));
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, const char* _value) -> XMLError
    {
        //return this->checkError(_element->Query(_value));
        return XMLError::OK;
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, double* _value) -> XMLError
    {
        return this->checkError(_element->QueryDoubleText(_value));
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, float* _value) -> XMLError
    {
        return this->checkError(_element->QueryFloatText(_value));
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, int* _value) -> XMLError
    {
        return this->checkError(_element->QueryIntText(_value));
    }

    template<>
    auto XML::queryText(tinyxml2::XMLElement* _element, int64_t* _value) -> XMLError
    {
        return this->checkError(_element->QueryInt64Text(_value));
    }

    template<typename TYPE>
    auto XML::getValueFromElement(std::string _name, TYPE* _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element = this->xmlRootNode->FirstChildElement(_name.c_str());

        if(element == nullptr)
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            error = this->queryText(element, _value);
        }

        return error;
    }

    template<typename TYPE>
    auto XML::getValueFromChildElement(std::string _fatherElement, std::string _childElement, TYPE* _value) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator fatherElement = this->getOpenedElement(_fatherElement);

        tinyxml2::XMLElement* element = fatherElement->second->FirstChildElement(_childElement.c_str());

        if((element == nullptr) || (fatherElement == this->openedElementsMap.end()))
        {
            error = XMLError::NULL_ELEMENT;
        }

        else
        {
            error = this->queryText(element, _value);

        }

        return error;
    }

    auto XML::deleteElement(std::string _childElement) -> XMLError
    {
        XMLError error = XMLError::OK;

        tinyxml2::XMLElement* element = this->xmlRootNode->FirstChildElement(_childElement.c_str());

        this->xmlRootNode->DeleteChild(element);

        return error;
    }

    auto XML::deleteChildElement(std::string _fatherElement, std::string _childElement) -> XMLError
    {
        XMLError error = XMLError::OK;

        std::map<std::string, tinyxml2::XMLElement*>::iterator fatherElement = this->getOpenedElement(_fatherElement);

        tinyxml2::XMLElement* element = fatherElement->second->FirstChildElement(_childElement.c_str());

        fatherElement->second->DeleteChild(element);

        return error;
    }
}
