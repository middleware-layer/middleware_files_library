/**
 ******************************************************************************
 * @file    XML.tpp
 * @author  leonardo
 * @version v1.0
 * @date    25 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_XML_XML_TPP
#define SOURCE_PRODUCTION_XML_XML_TPP

namespace Files
{
   //****************************************
   // Templated methods for writeElement() //
   //****************************************
   template auto XML::writeElement(std::string _label,
                                   bool _value) -> XMLError;

   template auto XML::writeElement(std::string _label,
                                   const char* _value) -> XMLError;

   template auto XML::writeElement(std::string _label,
                                   double _value) -> XMLError;

   template auto XML::writeElement(std::string _label,
                                   float _value) -> XMLError;

   template auto XML::writeElement(std::string _label,
                                   int _value) -> XMLError;

   template auto XML::writeElement(std::string _label,
                                   int64_t _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   bool _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   const char* _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   double _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   float _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   int _value) -> XMLError;

   template auto XML::writeElement(std::string _fatherName,
                                   std::string _name,
                                   int64_t _value) -> XMLError;

   //*********************************************************
   // Templated methods for writeAttributeToOpenedElement() //
   //*********************************************************
   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    bool _value) -> XMLError;

   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    const char* _value) -> XMLError;

   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    double _value) -> XMLError;

   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    float _value) -> XMLError;

   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    int _value) -> XMLError;

   template auto XML::writeAttributeToOpenedElement(std::string _fatherElement,
                                                    std::string _attributeName,
                                                    int64_t _value) -> XMLError;

   //**********************************************************
   // Templated methods for addChildElementToOpenedElement() //
   //**********************************************************
   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     bool _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     const char* _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     double _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     float _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     int _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     int64_t _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<bool> _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<const char*> _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<double> _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<float> _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<int> _value) -> XMLError;

   template auto XML::addChildElementToOpenedElement(std::string _fatherElement,
                                                     std::string _childElement,
                                                     std::vector<int64_t> _value) -> XMLError;

   //***********************************************
   // Templated methods for getValueFromElement() //
   //***********************************************
   template auto XML::getValueFromElement(std::string _name,
                                          bool* _value) -> XMLError;

   template auto XML::getValueFromElement(std::string _name,
                                          const char* _value) -> XMLError;

   template auto XML::getValueFromElement(std::string _name,
                                          double* _value) -> XMLError;

   template auto XML::getValueFromElement(std::string _name,
                                          float* _value) -> XMLError;

   template auto XML::getValueFromElement(std::string _name,
                                          int* _value) -> XMLError;

   template auto XML::getValueFromElement(std::string _name,
                                          int64_t* _value) -> XMLError;

   //****************************************************
   // Templated methods for getValueFromChildElement() //
   //****************************************************
   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               bool* _value) -> XMLError;

   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               const char* _value) -> XMLError;

   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               double* _value) -> XMLError;

   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               float* _value) -> XMLError;

   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               int* _value) -> XMLError;

   template auto XML::getValueFromChildElement(std::string _fatherElement,
                                               std::string _childElement,
                                               int64_t* _value) -> XMLError;

   //******************************************
   // Templated methods for queryAttribute() //
   //******************************************
   template auto XML::queryAttribute(tinyxml2::XMLElement* _element,
                                     std::string _name,
                                     bool* _value) -> XMLError;

   template auto XML::queryAttribute(tinyxml2::XMLElement* _element,
                                     std::string _name,
                                     double* _value) -> XMLError;

   template auto XML::queryAttribute(tinyxml2::XMLElement* _element,
                                     std::string _name,
                                     float* _value) -> XMLError;

   template auto XML::queryAttribute(tinyxml2::XMLElement* _element,
                                     std::string _name,
                                     int* _value) -> XMLError;

   template auto XML::queryAttribute(tinyxml2::XMLElement* _element,
                                     std::string _name,
                                     int64_t* _value) -> XMLError;
}

#endif // SOURCE_PRODUCTION_XML_XML_TPP
