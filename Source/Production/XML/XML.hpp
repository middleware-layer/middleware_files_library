/**
 ******************************************************************************
 * @file    XML.hpp
 * @author  leonardo
 * @version v1.0
 * @date    12 de jun de 2018
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_XML_XML_HPP
#define SOURCE_PRODUCTION_XML_XML_HPP

#include <memory>
#include <vector>
#include <map>

#include <Production/Features/FileSystem/Inc/File.h>
#include <Production/Features/FileSystem/Inc/FatFs/FATFileSystem.h>
#include <Production/XML/TinyXML2/tinyxml2.hpp>

namespace Files
{
    /**
     * @class   XML
     * @brief   For more information about Intel Hex format file, please access the link below:
     *          https://en.wikipedia.org/wiki/XML
     *
     *
     */
    class XML: public mbed::File
    {
        public:
            /**
             * @enum    XMLError
             * @brief
             *
             *
             */
            enum class XMLError
            {
                OK,   //!< No errors */
                NULL_ELEMENT,   //!< Null element */
                POORLY_CONSTRUCTED_FILE,   //!< XML file poorly constructed */
                NO_ATTRIBUTE,   //!< No attribute */
                FILE_NOT_FOUND,   //!< File not found */
                FILE_COULD_NOT_BE_OPENED,   //!< File could not be opened */
                FILE_READ_ERROR,   //!< File read error */
                ERROR_PARSING_ELEMENT,   //!< Error parsing element */
                ERROR_PARSING_ATTRIBUTE,   //!< Error parsing attribute */
                ERROR_PARSING_TEXT,   //!< Error parsing text */
                ERROR_PARSING_CDATA,   //!< Error parsing cdata */
                ERROR_PARSING_COMMENT,   //!< Error parsing comment */
                ERROR_PARSING_DECLARATION,   //!< Error parsing declaration */
                ERROR_PARSING_UNKNOWN,   //!< Error parsing unknown */
                ELEMENT_DEPTH_EXCEEDED,   //!< Element depth exceeded */
                UNDEFINED_ERROR,   //!< Undefined error */
            };

        private:
            /**
             * @var     fileSystem
             * @brief
             *
             *
             */
            std::shared_ptr<FATFileSystem> fileSystem;

            /**
             * @var
             * @brief
             *
             *
             */
            std::string filename;

            /**
             * @var     xmlDocument
             * @brief
             *
             *
             */
            std::shared_ptr<tinyxml2::XMLDocument> xmlDocument;

            /**
             * @var     xmlPrinter
             * @brief
             *
             *
             */
            std::shared_ptr<tinyxml2::XMLPrinter> xmlPrinter;

            /**
             * @var     xmlRootNode
             * @brief
             *
             *
             */
            tinyxml2::XMLNode* xmlRootNode;

            /**
             * @var
             * @brief
             *
             *
             */
            std::map<std::string, tinyxml2::XMLElement*> openedElementsMap;

            /**
             * @fn      checkError
             * @brief
             *
             *
             * @param   _error   :
             * @return
             */
            auto checkError(tinyxml2::XMLError _error) -> XMLError;

            /**
             * @fn      queryText
             * @brief
             *
             *
             * @param   _element :
             * @param   _value   :
             * @return
             */
            template<typename TYPE>
            auto queryText(tinyxml2::XMLElement* _element, TYPE* _value) -> XMLError;

            /**
             * @fn      queryAttribute
             * @brief
             *
             *
             * @param   _element :
             * @param   _name    :
             * @param   _value   :
             * @return
             */
            template<typename TYPE>
            auto queryAttribute(tinyxml2::XMLElement* _element, std::string _name, TYPE* _value) -> XMLError;

        protected:

        public:
            /**
             * @fn      XML
             * @brief
             *
             *
             * @param   _filename   :
             * @param   _flags      :
             */
            XML(std::string _filename, int _flags = O_RDONLY);

            /**
             * @fn      ~XML
             * @brief
             *
             *
             */
            virtual ~XML();

            /**
             * @fn      saveFile
             * @brief
             *
             *
             * @param   _compactMode   :
             * @return
             */
            auto saveFile(bool _compactMode = false) -> XMLError;

            /**
             * @fn      readFile
             * @brief
             *
             *
             * @return
             * @retval  XMLError_OK on success.
             */
            auto readFile(void) -> XMLError;

            /**
             * @fn      openXMLPrinter
             * @brief
             *
             *
             * @return
             * @retval  XMLError_OK on success.
             */
            auto openXMLPrinter(void) -> XMLError;

            /**
             * @fn      setRootNode
             * @brief
             *
             *
             * @param   _name :
             * @return
             */
            auto setRootNode(std::string _name) -> XMLError;

            /**
             * @fn      getRootNode
             * @brief
             *
             *
             * @return
             */
            auto getRootNode(void) -> tinyxml2::XMLNode*
            {
                return this->xmlRootNode;
            }

            /**
             * @fn      getOpenedElement
             * @brief
             *
             *
             * @param   _name :
             * @return
             */
            auto getOpenedElement(std::string _name) -> std::map<std::string, tinyxml2::XMLElement*>::iterator;

            /**
             * @fn      writeElement
             * @brief
             *
             *
             * @param   _name       :
             * @param   _value      :
             * @return
             * @retval  XMLError_OK on success.
             */
            template<typename TYPE>
            auto writeElement(std::string _name, TYPE _value) -> XMLError;

            /**
             * @fn      writeChildElement
             * @brief
             *
             *
             * @param   _fatherName :
             * @param   _name       :
             * @param   _value      :
             * @return
             * @retval  XMLError_OK on success.
             */
            template<typename TYPE>
            auto writeElement(std::string _fatherName, std::string _name, TYPE _value) -> XMLError;

            /**
             * @fn      createElement
             * @brief
             *
             *
             * @param   _name :
             * @return
             */
            auto createElement(std::string _name) -> XMLError;

            /**
             * @fn      openElement
             * @brief
             *
             *
             * @param   _name :
             * @return
             */
            auto openElement(std::string _name) -> XMLError;

            /**
             * @fn      openElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _name          :
             * @return
             */
            auto openElement(std::string _fatherElement, std::string _name) -> XMLError;

            /**
             * @fn      closeElement
             * @brief
             *
             *
             * @param   _name       :
             * @return
             */
            auto closeElement(std::string _name) -> XMLError;

            /**
             * @fn      closeElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _name          :
             * @return
             */
            auto closeElement(std::string _fatherElement, std::string _name) -> XMLError;

            /**
             * @fn      eraseElementFromMap
             * @brief
             *
             *
             * @param   _name :
             * @return
             */
            auto eraseElementFromMap(std::string _name) -> XMLError;

            /**
             * @fn      writeAttributeToOpenedElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _attributeName :
             * @param   _value         :
             * @return
             */
            template<typename TYPE>
            auto writeAttributeToOpenedElement(std::string _fatherElement, std::string _attributeName,
                    TYPE _value) -> XMLError;

            /**
             * @fn      addChildElementToOpenedElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _childElement  :
             * @param   _value         :
             * @return
             */
            template<typename TYPE>
            auto addChildElementToOpenedElement(std::string _fatherElement, std::string _childElement,
                    TYPE _value) -> XMLError;

            /**
             * @fn      addChildElementToOpenedElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _childElement  :
             * @param   _value         :
             * @return
             */
            template<typename TYPE>
            auto addChildElementToOpenedElement(std::string _fatherElement, std::string _childElement,
                    std::vector<TYPE> _value) -> XMLError;

            /**
             * @fn      getValueFromElement
             * @brief
             *
             *
             * @param   _name    :
             * @param   _value   :
             * @return
             */
            template<typename TYPE>
            auto getValueFromElement(std::string _name, TYPE* _value) -> XMLError;

            /**
             * @fn      getValueFromChildElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _childElement  :
             * @param   _value         :
             * @return
             */
            template<typename TYPE>
            auto getValueFromChildElement(std::string _fatherElement, std::string _childElement,
                    TYPE* _value) -> XMLError;

            template<typename TYPE>
            auto getValueFromAttribute() -> XMLError;

            /**
             * @fn      deleteElement
             * @brief
             *
             *
             * @param   _childElement  :
             * @return
             */
            auto deleteElement(std::string _childElement) -> XMLError;

            /**
             * @fn      deleteChildElement
             * @brief
             *
             *
             * @param   _fatherElement :
             * @param   _childElement  :
             * @return
             */
            auto deleteChildElement(std::string _fatherElement, std::string _childElement) -> XMLError;
    };
}

#endif // SOURCE_PRODUCTION_XML_XML_HPP
